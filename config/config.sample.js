const config = {
  server: process.env.NEXT_PUBLIC_S3APP_SERVER || '',
  demo: false,
  license: false,
  noConfirmation: false,
};

export default config;
