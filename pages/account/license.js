import { useSnackbar } from 'notistack';
import { gql, useMutation } from '@apollo/client';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { useTheme } from '@mui/material/styles';
import { useState } from 'react';
import { useRouter } from 'next/router';

const UPDATE_LICENSE = gql`
mutation($license: String!) {
    updateLicense(license: $license)
}
`;

export default function License(props) {
  const router = useRouter();
  const theme = useTheme();

  const [licenseForm, setLicenseForm] = useState({
    license: '',
  });
  const [updateLicense] = useMutation(UPDATE_LICENSE);
  const { enqueueSnackbar } = useSnackbar();

  return (
    <Container
      maxWidth="xs"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mb: `${theme.variables.header.height}px`,
        }}
      >
        <Typography variant="h5">
          Ввод лицензионного ключа
        </Typography>
        <Box sx={{ mt: 1, textAlign: 'center' }}>
          <TextField
            margin="normal"
            fullWidth
            label="Лицензионный ключ"
            name="name"
            value={licenseForm.license}
            onChange={(e) => setLicenseForm({ ...licenseForm, license: e.target.value })}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 1 }}
            disabled={
              !licenseForm.license
            }
            onClick={async () => {
              try {
                await updateLicense({
                  variables: { ...licenseForm },
                  onCompleted: async (data) => {
                    console.log(data);
                    if (data.updateLicense) {
                      enqueueSnackbar('Лицензия обновлена', {
                        variant: 'success',
                      });
                      await props.refetchUser();
                      router.push('/');
                    } else {
                      enqueueSnackbar('Лицензионный ключ неверен', {
                        variant: 'error',
                      });
                    }
                    // try {
                    //   signIn({
                    //     variables: { ...registerForm },
                    //     onCompleted: (data) => {
                    //       localStorage.setItem('token', data.signIn);
                    //       props.refetchUser();
                    //     },
                    //   });
                    // } catch (e) {
                    //   enqueueSnackbar(e.message, { variant: 'error' });
                    // }
                  },
                });
              } catch (e) {
                enqueueSnackbar(e.message, { variant: 'error' });
              }
            }}
          >
            Активировать ключ
          </Button>
        </Box>
      </Box>
    </Container>
  );
}
