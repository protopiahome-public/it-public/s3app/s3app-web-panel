import { useSnackbar } from 'notistack';
import { gql, useMutation } from '@apollo/client';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { useState } from 'react';
import { useRouter } from 'next/router';
import config from '../config/config';

const SIGN_IN = gql`
  mutation ($password: String!, $email: String, $name: String) {
    signIn(email: $email, name: $name, password: $password)
  }
`;

export default function Login(props) {
  const router = useRouter();
  const [signIn] = useMutation(SIGN_IN);
  const { enqueueSnackbar } = useSnackbar();

  const [loginForm, setLoginForm] = useState({
    email: '',
    password: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setLoginForm({ ...loginForm, [name]: value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await signIn({ variables: loginForm });
      enqueueSnackbar('Вы вошли', { variant: 'success' });
      localStorage.setItem('token', response.data.signIn);
      props.refetchUser();
    } catch (error) {
      enqueueSnackbar(error.message, { variant: 'error' });
    }
  };

  if (props?.user?.domains.length > 0) {
    router.push(`/domains/${props.user.domains[0].id}`);
  }

  if (props?.user?.domains.length === 0) {
    if (config.demo) router.push('/');
    else router.push('/welcomePage');
  }

  return (
    <Container className="flex flex-col items-center justify-center max-w-lg pb-18">
      <Typography variant="h5">Вход</Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          margin="normal"
          fullWidth
          label="E-Mail"
          type="email"
          name="email"
          value={loginForm.email}
          onChange={handleInputChange}
          required
        />
        <TextField
          margin="normal"
          fullWidth
          label="Пароль"
          type="password"
          name="password"
          value={loginForm.password}
          onChange={handleInputChange}
          required
        />
        <Button
          fullWidth
          variant="contained"
          type="submit"
          className="mt-1"
          disabled={!loginForm.email || !loginForm.password}
        >
          Войти
        </Button>
      </form>
      <div className="flex w-full justify-between pt-2 text-sm">
        <Link href="restore-password/request" className="no-underline">
          Забыли пароль?
        </Link>
        <Link href="register" className="no-underline">
          Зарегистрироваться
        </Link>
      </div>
    </Container>
  );
}
