import App from 'next/app';
import { ApolloProvider, gql, useQuery } from '@apollo/client';
import Head from 'next/head';
import {
  createTheme,
  ThemeProvider,
  StyledEngineProvider,
} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Link from 'next/link';
import { Box, IconButton } from '@mui/material';
import { useRouter } from 'next/router';
import Button from '@mui/material/Button';
import { useRef, useState } from 'react';
import Popover from '@mui/material/Popover';
import { SnackbarProvider, useSnackbar } from 'notistack';
import client from '../components/apollo-client';
import Logo from '../assets/logo.svg';
import Dropdown from '../assets/dropdown.svg';
import S3Avatar from '../components/S3Avatar';
import SelectDomain from '../components/SelectDomain';
import config from '../config/config';
import '../globals.css';

function DropdownIcon(props) {
  return (
    <Box
      {...props}
      component="span"
      sx={{
        px: 0.5,
        stroke: '#859CBE',
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <Dropdown />
    </Box>
  );
}

const theme = createTheme({
  palette: {
    primary: {
      main: '#F79244',
      contrastText: '#ffffff',
    },
    secondary: {
      main: '#DAE5F4',
      contrastText: '#4B5A73',
    },
    tertiary: {
      main: '#687B98',
      contrastText: '#4B5A73',
    },
  },
  variables: {
    leftPanel: {
      width: 250,
      minWidth: 250,
    },
    header: {
      height: 73,
    },
  },
  components: {
    MuiSelect: {
      defaultProps: {
        IconComponent: DropdownIcon,
      },
      styleOverrides: {
        select: {
          '&:focus': {
            backgroundColor: 'transparent',
          },
        },
      },
    },
  },
});

const GET_DOMAIN = gql`
  query GetDomain($id: ID!) {
    getDomain(id: $id) {
      parentDomain {
        domainType
        id
        name
      }
      id
      name
      domainType
    }
  }
`;

const GET_ME = gql`
  query {
    me {
      id
      name
      licenseExpires
      domains {
        id
        name
        domainType
        subDomains {
          id
          name
        }
      }
    }
    getDomains {
      id
      name
      domainType
      subDomains {
        id
        name
        participants {
          id
        }
      }
    }
  }
`;

function S3App({ Component, pageProps }) {
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();
  try {
    window.enqueueSnackbar = enqueueSnackbar;
    window.router = router;
  } catch (e) {
    //
  }

  const userPanelRef = useRef(null);
  const [userPanelOpen, setUserPanelOpen] = useState(false);

  const getMe = useQuery(GET_ME);
  let { data } = getMe;
  const { refetch, error, loading } = getMe;
  if (!loading && (error || !data?.me)) {
    localStorage.removeItem('token');
    data = null;
  }

  const sortedDomains = [];

  data?.getDomains.forEach((domain) => {
    if (domain.domainType === 'organization') sortedDomains.push(domain);
  });

  const getOrganization = useQuery(GET_DOMAIN, {
    variables: { id: router.query.id },
    skip: !router.query.id,
  });

  if (getOrganization.loading) return null;

  if (
    getOrganization.data?.getDomain.domainType !== 'organization'
    && getOrganization.data?.getDomain.parentDomain?.id
  ) {
    getOrganization.refetch({
      id: getOrganization.data?.getDomain.parentDomain?.id,
    });
  }

  const allowedPathsGuest = [
    '/login',
    '/register',
    '/restore-password/request',
    '/restore-password/confirm',
    '/confirm-email',
  ];
  const disallowedPathsUser = [
    '/login',
    '/register',
    '/restore-password/request',
    '/restore-password/confirm',
  ];

  if (loading) {
    return null;
  }

  if (
    !loading
    && !data?.me
    && !allowedPathsGuest.some((path) => router.pathname.includes(path))
  ) {
    router.push('/login');
    return null;
  }

  if (
    !loading
    && data
    && disallowedPathsUser.some((path) => router.pathname.includes(path))
  ) {
    router.push('/');
    return null;
  }

  if (
    !config.demo
    && data?.me
    && !data?.getDomains?.length
    && !router.pathname.includes('/welcomePage')
  ) {
    router.push('/welcomePage');
    return null;
  }

  if (
    config.license
    && data?.me
    && new Date(data?.me.licenseExpires) < new Date()
    && !router.pathname.includes('/account/license')
  ) {
    router.push('/account/license');
    return null;
  }

  return (
    <>
      {
      // TODO: Move to globals.css
      }
      <style>
        {`
            html, body {
              height: 100%
            }
            .issuesTable .MuiTableCell-root {
              border: 1px solid #DAE5F4;
            }
            #__next, #app, main, main > div {
              height: 100%
            }
            #__next > div {
              display: flex;
              flex-direction: column;
            }
            body {
              font-family: 'Roboto', sans-serif;
              font-size: 12px;
              margin: 0;
            }
            a {
              color: inherit;
              text-decoration: none;
            }
            h2 { font-size: 20px; }
            h3 { font-size: 16px; }
            h4 { font-size: 14px; }
            .domainTable > tbody > tr > td {
              padding-bottom: 20px;
            }
            .mdxeditor-popup-container {
              z-index: 2000;
            }
            .mdxeditor a {
              color: rgb(104, 123, 152);
              text-decoration: underline;
            }
      `}
      </style>
      <div id="app">
        <Head>
          <title>S3App</title>
          <link rel="icon" href="/favicon.jpg" />
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
          />
          <meta name="viewport" content="width=1200" />
        </Head>
        <Box
          sx={{
            fontSize: '16px',
            display: 'flex',
            alignItems: 'center',
            borderBottom: data?.me ? '#687B98 1px solid' : 'none',
            height: theme.variables.header.height - 1,
          }}
        >
          <IconButton disableRipple href="/">
            <Logo style={{ marginLeft: '15px' }} />
          </IconButton>

          {data?.me && (
            <Box sx={{ display: 'flex', width: '100%', alignItems: 'center' }}>
              <Box
                sx={{
                  color: '#859CBE',
                  marginLeft: '105px',
                  display: 'inline-flex',
                  alignItems: 'center',
                }}
              >
                <span
                  style={{
                    color: theme.palette.primary.main,
                    marginRight: 22,
                    whiteSpace: 'pre',
                  }}
                >
                  {getOrganization.data?.getDomain.name}
                </span>
                <SelectDomain
                  sx={{ minWidth: 250 }}
                  value={router.query.id}
                  variant="outlined"
                  size="small"
                  user={data?.me.id}
                  onChange={(e) => router.push(`/domains/${e.target.value}`)}
                />
              </Box>
              <Box sx={{ flex: 1 }}>
                {Component.Header ? (
                  <Component.Header
                    user={data?.me}
                    refetchUser={refetch}
                    refetchDomain={() => refetch()}
                    organization={getOrganization.data?.getDomain}
                  />
                ) : null}
              </Box>

              <Link href="/account">
                <S3Avatar name={data?.me.name} sx={{ cursor: 'pointer' }} />
              </Link>
              <Link href="/account">
                <span
                  style={{
                    color: theme.palette.primary.main,
                    marginLeft: 8,
                    cursor: 'pointer',
                  }}
                >
                  {data?.me.name}
                </span>
              </Link>
              <IconButton
                ref={userPanelRef}
                sx={{ stroke: theme.palette.primary.main, marginRight: 2 }}
                onClick={() => setUserPanelOpen(!userPanelOpen)}
              >
                <Dropdown />
              </IconButton>
              <Popover
                open={userPanelOpen}
                anchorEl={userPanelRef.current}
                onClose={() => setUserPanelOpen(false)}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
              >
                <Button
                  variant="contained"
                  color="primary"
                  onClick={async () => {
                    setUserPanelOpen(false);
                    localStorage.removeItem('token');
                    try {
                      refetch();
                    } catch (e) {
                      console.log(e);
                    }
                    router.push('/login');
                  }}
                >
                  Выйти
                </Button>
              </Popover>
            </Box>
          )}
        </Box>
        <Component
          {...pageProps}
          user={data?.me}
          refetchUser={refetch}
          refetchDomain={() => refetch()}
          organization={getOrganization.data?.getDomain}
        />
      </div>
    </>
  );
}

export default function S3AppContainer(props) {
  return (
    <StyledEngineProvider injectFirst>
      <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
          <SnackbarProvider maxSnack={3}>
            <CssBaseline />
            <S3App {...props} />
          </SnackbarProvider>
        </ThemeProvider>
      </ApolloProvider>
    </StyledEngineProvider>
  );
}

S3AppContainer.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);
  return { ...appProps };
};
