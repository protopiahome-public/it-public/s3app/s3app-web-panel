import { useSnackbar } from 'notistack';
import { gql, useMutation } from '@apollo/client';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { useState } from 'react';
import { useRouter } from 'next/router';
import config from '../config/config';

const SIGN_UP = gql`
  mutation($name: String!, $email: String!, $password: String!${
  config.license ? ', $license: String' : ''
}) {
    signUp(name: $name, email: $email, password: $password${
  config.license ? ', license: $license' : ''
}) {
      id
    }
  }
`;

export default function Register() {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();

  const [registerForm, setRegisterForm] = useState({
    name: '',
    email: '',
    password: '',
    license: '',
  });
  const [signUp] = useMutation(SIGN_UP);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await signUp({
        variables: { ...registerForm },
        onCompleted: () => {
          enqueueSnackbar(
            config.noConfirmation ? 'Регистрация прошла успешно.' : 'Регистрация прошла успешно. Пожалуйста, подтвердите свою почту.',
            {
              variant: 'success',
            },
          );
          router.push('/login');
        },
      });
    } catch (error) {
      enqueueSnackbar(error.message, { variant: 'error' });
    }
  };

  return (
    <Container className="flex flex-col items-center justify-center max-w-lg pb-18">
      <form
        onSubmit={handleSubmit}
        className="flex flex-col items-center justify-center"
      >
        <div className="flex flex-col items-center">
          <Typography variant="h5">Регистрация</Typography>
          <div className="text-center">
            <TextField
              margin="normal"
              fullWidth
              label="Имя"
              name="name"
              value={registerForm.name}
              onChange={(e) => setRegisterForm({ ...registerForm, name: e.target.value })}
            />
            <TextField
              margin="normal"
              fullWidth
              label="E-Mail"
              name="email"
              type="email"
              value={registerForm.email}
              onChange={(e) => setRegisterForm({ ...registerForm, email: e.target.value })}
            />
            <TextField
              margin="normal"
              fullWidth
              label="Пароль"
              type="password"
              value={registerForm.password}
              onChange={(e) => setRegisterForm({ ...registerForm, password: e.target.value })}
            />
            {config.license && (
              <TextField
                margin="normal"
                fullWidth
                label="Лицензионный ключ"
                name="license"
                value={registerForm.license}
                onChange={(e) => setRegisterForm({ ...registerForm, license: e.target.value })}
              />
            )}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className="mt-4"
              disabled={
                !registerForm.name
                || !registerForm.email
                || !registerForm.password
                || (config.license && !registerForm.license)
              }
            >
              Зарегистрироваться
            </Button>
            <div className="flex w-full justify-center gap-2 pt-2 text-sm">
              <span>Уже есть аккаунт?</span>
              <Link href="login" className="no-underline">
                Войти
              </Link>
            </div>
          </div>
        </div>
      </form>
    </Container>
  );
}
