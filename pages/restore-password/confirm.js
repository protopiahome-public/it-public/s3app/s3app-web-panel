import {
  Button, Container, TextField, Typography,
} from '@mui/material';
import Link from '@mui/material/Link';
import { gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useSnackbar } from 'notistack';

const CHANGE_PASSWORD_AFTER_RECOVER = gql`
  mutation ($code: ID!, $password: String!) {
    changePasswordAfterRecover(code: $code, password: $password)
  }
`;

export default function Confirm() {
  const { enqueueSnackbar } = useSnackbar();
  const [changePasswordAfterRecover] = useMutation(
    CHANGE_PASSWORD_AFTER_RECOVER,
  );
  const [form, setForm] = useState({
    password: '',
    passwordConfirm: '',
  });

  const code = new URLSearchParams(window.location.search).get('code');

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await changePasswordAfterRecover({
        variables: { code, password: form.password },
      });
      enqueueSnackbar('Пароль успешно изменен', { variant: 'success' });
    } catch (error) {
      enqueueSnackbar(error.message, { variant: 'error' });
    }
  };

  return (
    <Container className="flex flex-col items-center justify-center max-w-lg pb-18">
      <form
        onSubmit={handleSubmit}
        className="flex flex-col items-center justify-center"
      >
        <div className="flex flex-col items-center">
          <Typography variant="h5">Восстановление пароля</Typography>
          <div className="mt-4 text-center">
            <TextField
              margin="normal"
              fullWidth
              label="Пароль"
              type="password"
              value={form.password}
              onChange={(e) => setForm({ ...form, password: e.target.value })}
            />
            <TextField
              margin="normal"
              fullWidth
              label="Подтверждение пароля"
              type="password"
              value={form.passwordConfirm}
              onChange={(e) => setForm({ ...form, passwordConfirm: e.target.value })}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              className="mt-4"
              disabled={
                !form.password
                || !form.passwordConfirm
                || form.password !== form.passwordConfirm
              }
            >
              Сменить пароль
            </Button>
            <div className="flex justify-between pt-2 text-base">
              <Link href="/login" className="no-underline">
                Войти
              </Link>
              <Link href="/register" className="no-underline">
                Зарегистрироваться
              </Link>
            </div>
          </div>
        </div>
      </form>
    </Container>
  );
}
