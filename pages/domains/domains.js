import { gql, useQuery } from '@apollo/client';
import Head from 'next/head';

import LeftSubPanel from '../../components/LeftSubPanel';

const GET_DOMAINS = gql`
query getDomains {
  getDomains {
    id
    name
  }
}
  `;

function Domains() {
  const { loading } = useQuery(GET_DOMAINS);
  if (loading) {
    return null;
  }

  return (
    <>
      <Head>
        <title>Домены</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены" />
        <div style={{ flex: 1, paddingLeft: 36 }}>
          В разработке
        </div>
      </div>
    </>
  );
}

export default Domains;
