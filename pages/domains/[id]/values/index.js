import Head from 'next/head';
import { useState } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { MuiColorInput } from 'mui-color-input';
import { Checkbox, FormControlLabel } from '@mui/material';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import { checkEmpty } from '../../../../components/common';

const GET_STAGES = gql`
query GetDomain($getDomainId: ID!) {
  getDomain(id: $getDomainId) {
    stages {
      id
      name
      color
      isStart
      isEnd
    }
  }
}
`;

const CREATE_STAGE = gql`
mutation CreateStage($stage: StageInput!) {
  createStage(stage: $stage) {
    id
  }
}`;

const DELETE_STAGE = gql`
mutation Mutation($deleteStageId: ID!) {
  deleteStage(id: $deleteStageId)
}`;

const EDIT_STAGE = gql`
mutation Mutation($editStageId: ID!, $stage: StageInput!) {
  editStage(id: $editStageId, stage: $stage) {
    id
  }
}`;

const ORDER_STAGES = gql`
mutation Mutation($stageIds: [ID]!) {
  orderStages(stageIds: $stageIds) {
    id
    name
  }
}`;

function Values() {
  const router = useRouter();

  const { enqueueSnackbar } = useSnackbar();

  const { data, refetch } = useQuery(GET_STAGES, { variables: { getDomainId: router.query.id } });

  const [selectedStage, setSelectedStage] = useState('');

  const [createStage] = useMutation(CREATE_STAGE);
  const [editStage] = useMutation(EDIT_STAGE);
  const [deleteStage] = useMutation(DELETE_STAGE);
  const [orderStages] = useMutation(ORDER_STAGES);

  const [stageDialog, setStageDialog] = useState({ open: false, newStage: '' });

  const handleListItemClick = (event, stage) => {
    setSelectedStage({ ...stage });
  };

  const onDragEnd = async (result) => {
    if (!result.destination) {
      return;
    }

    const stages = Array.from(data?.getDomain?.stages);
    const [removed] = stages.splice(result.source.index, 1);
    stages.splice(result.destination.index, 0, removed);

    await orderStages({
      variables: {
        stageIds: stages.map((stage) => stage.id),
      },
    });
    refetch();
  };

  return (
    <>
      <Head>
        <title>Создание ценности</title>
      </Head>
      <div style={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="values" />
        </LeftSubPanel>
        <div>
          <Box sx={{ marginTop: 3, marginLeft: 3, display: 'flex' }}>
            <Box sx={{ width: 250 }}>
              <List
                sx={{
                  bgcolor: 'background.paper',
                  position: 'relative',
                  overflow: 'auto',
                  maxHeight: 300,
                  '& ul': { padding: 0 },
                  border: '2px solid #F79244',
                  borderRadius: 1,
                  padding: 0,
                }}
              >
                <DragDropContext onDragEnd={onDragEnd}>
                  <Droppable droppableId="droppable">
                    {(provided) => (
                      <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                      >
                        {data?.getDomain?.stages.length ? (
                          data.getDomain?.stages.map((stage, index) => (
                            <Draggable key={stage.id} draggableId={stage.id} index={index}>
                              {(_provided) => (
                                <div
                                  ref={_provided.innerRef}
                                  {..._provided.draggableProps}
                                  {..._provided.dragHandleProps}
                                >
                                  <ListItemButton
                                    selected={selectedStage.id === stage.id}
                                    key={stage.id}
                                    onClick={(event) => handleListItemClick(event, stage)}
                                  >
                                    <ListItemText primary={stage.name} />
                                  </ListItemButton>
                                </div>
                              )}
                            </Draggable>
                          ))
                        ) : (
                          <ListItemButton disabled>
                            <ListItemText primary="Этапов нет" />
                          </ListItemButton>
                        )}
                      </div>
                    )}
                  </Droppable>
                </DragDropContext>
              </List>
              <Button
                variant="contained"
                sx={{ marginTop: 1, width: '100%' }}
                onClick={() => {
                  setStageDialog({ open: true });
                }}
              >
                Создать этап
              </Button>
            </Box>
            <Box
              sx={{
                marginLeft: 1,
                display: selectedStage ? 'block' : 'none',
                maxWidth: 370,
              }}
            >
              <div>
                <TextField
                  required
                  error={!selectedStage.name}
                  variant="outlined"
                  size="small"
                  label="Название"
                  value={selectedStage.name}
                  sx={{ width: '100%' }}
                  onChange={(e) => setSelectedStage({ ...selectedStage, name: e.target.value })}
                />
              </div>
              <div>
                <MuiColorInput
                  label="Цвет"
                  value={selectedStage.color || ''}
                  onChange={(color) => setSelectedStage({ ...selectedStage, color })}
                  variant="outlined"
                  size="small"
                  sx={{ width: '100%' }}
                />
              </div>
              <div>
                <FormControlLabel
                  control={(
                    <Checkbox
                      checked={selectedStage.isStart || false}
                      onChange={(e) => setSelectedStage({
                        ...selectedStage,
                        isStart: e.target.checked,
                        isEnd: e.target.checked ? false : selectedStage.isEnd,
                      })}
                    />
)}
                  label="Начальный этап"
                />
              </div>
              <div>
                <FormControlLabel
                  control={(
                    <Checkbox
                      checked={selectedStage.isEnd || false}
                      onChange={(e) => setSelectedStage({
                        ...selectedStage,
                        isEnd: e.target.checked,
                        isStart: e.target.checked ? false : selectedStage.isStart,
                      })}
                    />
)}
                  label="Конечный этап"
                />
              </div>
              <Button
                disabled={checkEmpty(selectedStage.name)}
                variant="contained"
                sx={{ marginTop: 1, width: '100%' }}
                onClick={async () => {
                  try {
                    await editStage({
                      variables: {
                        editStageId: selectedStage.id,
                        stage: {
                          name: selectedStage.name,
                          color: selectedStage.color,
                          isStart: selectedStage.isStart,
                          isEnd: selectedStage.isEnd,
                        },
                      },
                    }).then(() => {
                      refetch();
                    });
                  } catch (e) {
                    console.log(e);
                    enqueueSnackbar(e.message, { variant: 'error' });
                    return;
                  }
                  enqueueSnackbar('Этап успешно отредактирован', { variant: 'success' });
                }}
              >
                Сохранить
              </Button>
              <Button
                variant="contained"
                sx={{
                  marginTop: 1, width: '100%', background: 'red', '&:hover': { background: 'firebrick' },
                }}
                onClick={async () => {
                  try {
                    await deleteStage({
                      variables: {
                        deleteStageId: selectedStage.id,
                      },
                    }).then(() => {
                      refetch();
                    });
                  } catch (e) {
                    console.log(e);
                    enqueueSnackbar(e.message, { variant: 'error' });
                    return;
                  }
                  setSelectedStage('');
                  enqueueSnackbar('Этап успешно удален', { variant: 'success' });
                }}
              >
                Удалить
              </Button>
            </Box>
          </Box>
          <SimpleDialog
            disabled={!stageDialog.stage}
            open={stageDialog.open}
            title="Создание этапа"
            text="Этап - часть пути формирования конечного результата"
            acceptText="Создать"
            cancelText="Отмена"
            onCancel={() => setStageDialog({})}
            onAccept={async () => {
              try {
                await createStage({
                  variables: {
                    stage: { domainId: router.query.id, name: stageDialog.stage },
                  },
                }).then(() => {
                  refetch();
                  setStageDialog({});
                });
              } catch (e) {
                console.log(e);
                enqueueSnackbar(e.message, { variant: 'error' });
                return;
              }
              enqueueSnackbar('Этап успешно создан', { variant: 'success' });
              setStageDialog({});
            }}
          >
            <TextField
              error={checkEmpty(stageDialog.stage)}
              autoFocus
              margin="dense"
              id="name"
              label="Название"
              type="text"
              fullWidth
              variant="standard"
              onChange={(e) => setStageDialog({ ...stageDialog, stage: e.target.value })}
            />
          </SimpleDialog>
        </div>
      </div>
    </>
  );
}

export default Values;
