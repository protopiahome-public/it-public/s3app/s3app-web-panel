import Head from 'next/head';
import {
  Box,
  Button,
  MenuItem,
  Select,
  TextField,
  useTheme,
  IconButton,
  Autocomplete,
  Dialog,
  DialogContent,
  LinearProgress,
} from '@mui/material';
import { useQuery, gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { Clear, Lock, PlayArrow } from '@mui/icons-material';
import moment from 'moment';
import { checkEmpty, priorities, priorityShield } from '../../../../components/common';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';

import Delete from '../../../../assets/delete.svg';
import Close from '../../../../assets/close.svg';
import SimpleDialog from '../../../../components/SimpleDialog';
import S3Avatar from '../../../../components/S3Avatar';
import MarkdownEditorLoaded from '../../../../components/MarkdownEditorLoaded';

const GET_USERS = gql`
  query getUsersOfOrganization($domainId: ID!) {
    getUsersOfOrganization(domainId: $domainId) {
      id 
      name
    }
  }
`;

const GET_ISSUES = gql`
  query getIssuesOfOrganization($domainId: ID!) {
    getIssuesOfOrganization(domainId: $domainId) {
      id 
      name
    }
  }
`;

const GET_DOMAINS = gql`
  query getDomainsOfOrganization($domainId: ID!) {
    getDomainsOfOrganization(domainId: $domainId) {
      id
      name
    }
  }
`;

const GET_DRIVERS = gql`
  query getDriversOfOrganization($domainId: ID!) {
    getDriversOfOrganization(domainId: $domainId) {
      id 
      name
    }
  }
`;

const CREATE_ISSUE = gql`
  mutation createIssue($issue: IssueInput!) {
    createIssue(issue: $issue) {
      id
    }
  }
`;

const EDIT_ISSUE = gql`
  mutation editIssue($id: ID!, $issue: IssueUpdateInput!) {
    editIssue(id: $id, issue: $issue) {
      id
    }
  }
`;

const DELETE_ISSUE = gql`
  mutation deleteIssue($id: ID!) {
    deleteIssue(id: $id)
  }
`;

const GET_ISSUE = gql`
  query getIssue($id: ID!) {
    getIssue(id: $id) {
      id
      name
      description
      issueLinks {
        id
        name
      }
      drivers {
        id
        name
      }
      priority
      createdAt
      issueType {
        id
        name
      }
      stage {
        id
        name
      }
      author {
        id
        name
      }
      historyLog {
        date
        name
        description
        type
      }
      domain {
        name
        id
      }
      executor {
        id
        name
      }
      sprint {
        id
      }
      version
      stage {
        id
      }
      isClosed
      closedAt
      deadlineAt
    }
  }
`;

const GET_STAGES = gql`
query GetDomain($getDomainId: ID!) {
  getDomain(id: $getDomainId) {
    stages {
      id
      name
      isStart
      isEnd
    }
    issueTypes {
      id
      name
      stages {
        id
        name
        isStart
        isEnd
      }
    }
  }
}
`;

const styles = {
  fields: {
    '&': {
      paddingTop: '10px',
      display: 'grid',
      gridTemplateColumns: '1fr 1fr',
      width: '100%',
      columnGap: '20px',
    },
    '& > div': {
      display: 'flex',
      alignItems: 'center',
    },
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  description: {
    border: '2px solid #A2BDE4',
    borderRadius: '5px',
    padding: '20px',
  },
  image: {
    marginLeft: '4px',
    borderRadius: '50%',
    borderWidth: '2px',
    borderStyle: 'solid',
    borderColor: (theme) => theme.palette.primary.main,
  },
};

export default function Issue(props) {
  const theme = useTheme();

  const router = useRouter();

  const domainId = props.isDialog ? props.domainId : router.query.id;
  const issueId = props.isDialog ? props.issueId : router.query['issue-id'];

  const isNew = () => !issueId;

  const stages = useQuery(GET_STAGES, { variables: { getDomainId: domainId } });
  const issues = useQuery(GET_ISSUES, { variables: { domainId } });
  const users = useQuery(GET_USERS, { variables: { domainId } });
  const domains = useQuery(GET_DOMAINS, { variables: { domainId } });
  const drivers = useQuery(GET_DRIVERS, { variables: { domainId: router.query.id } });

  const [save] = useMutation(CREATE_ISSUE);
  const [edit] = useMutation(EDIT_ISSUE);
  const [remove] = useMutation(DELETE_ISSUE);

  const [issue, setIssue] = useState({
    name: '',
    description: '',
    priority: 0,
    executorUserId: null,
    domainId,
    stageId: null,
    issueTypeId: null,
    deadlineAt: null,
  });

  const { data } = useQuery(GET_ISSUE, {
    variables: { id: issueId },
    skip: !issueId,
    onCompleted: (_data) => {
      stages.refetch({ getDomainId: _data.getIssue.domain?.id });
      setIssue({
        name: _data.getIssue.name,
        description: _data.getIssue.description,
        priority: _data.getIssue.priority,
        executorUserId: _data.getIssue.executor?.id,
        issueLinks: _data.getIssue.issueLinks?.map((issueLink) => issueLink.id) || [],
        driverIds: _data.getIssue.drivers?.map((driver) => driver.id) || [],
        stageId: _data.getIssue?.stage?.id,
        issueTypeId: _data.getIssue.issueType?.id,
        domainId: _data.getIssue.domain?.id,
        authorUserId: _data.getIssue.author?.id,
        deadlineAt: _data.getIssue.deadlineAt,
      });
    },
  });

  const [deleteDialog, setDeleteDialog] = useState(false);
  const [closeDialog, setCloseDialog] = useState(false);
  const [isChanged, setIsChanged] = useState(false);

  if (issues.loading || users.loading || domains.loading || stages.loading || drivers.loading) {
    if (props.isDialog) {
      return (
        <Dialog
          open={props.open}
          onClose={props.onClose}
          maxWidth="lg"
          fullWidth
        >
          <DialogContent>
            <LinearProgress />
          </DialogContent>
        </Dialog>
      );
    }
    return null;
  }

  let filteredStages = stages.data?.getDomain?.stages || [];
  if (issue.issueTypeId) {
    filteredStages = stages.data?.getDomain?.issueTypes.find(
      (issueType) => issueType.id === issue.issueTypeId,
    )?.stages || [];
  }

  const close = () => {
    if (isChanged) {
      setCloseDialog(true);
    } else if (props.isDialog) {
      props.onClose();
    } else {
      router.push(`${router.asPath}/../`);
    }
  };

  const canSubmit = !(checkEmpty(issue.name) || checkEmpty(issue.priority)
  || checkEmpty(issue.stageId) || checkEmpty(issue.domainId));

  const result = (
    <div style={{ display: 'flex' }}>
      {props.isDialog ? null
        : (
          <>
            <Head>
              <title>
                {issueId ? `Проблема ${issue.name}` : 'Новая проблема'}
              </title>
            </Head>
            <LeftSubPanel title="Домены">
              <DomainLeftPanel selected="issues" />
            </LeftSubPanel>
          </>
        )}
      <Box sx={{
        flex: 1, mx: 4, display: 'flex', flexDirection: 'column',
      }}
      >
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%',
              borderBottom: `${theme.palette.primary.main} solid 2px`,
            }}
          >
            <span style={styles.fieldTitle}>Название*</span>
            <TextField
              value={issue.name}
              error={checkEmpty(issue.name)}
              size="normal"
              variant="standard"
              color="secondary"
              fullWidth
              sx={checkEmpty(issue.name) ? undefined : {
                '& .MuiInput-underline::before': {
                  display: 'none',
                },
                '& .MuiInput-underline::after': {
                  display: 'none',
                },
              }}
              onChange={(e) => {
                setIssue({
                  ...issue,
                  name: e.target.value,
                });
                setIsChanged(true);
              }}
            />

            {!isNew()
                && (
                <IconButton onClick={() => setDeleteDialog(true)}>
                  <Delete
                    style={{ stroke: theme.palette.primary.main }}
                  />
                </IconButton>
                )}

            <IconButton onClick={() => {
              close();
            }}
            >
              <Close
                style={{ stroke: theme.palette.primary.main }}
              />
            </IconButton>
          </Box>
        </Box>
        <div style={{ display: 'flex', marginBottom: '20px' }}>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Создатель
              </span>
              <span style={styles.fieldValue}>
                <S3Avatar name={data?.getIssue.author?.name} size={30} sx={{ ml: 1 }} noBorder color="#687B98" />
              </span>
              {data?.getIssue.author?.name}
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Связь
              </span>
              <Autocomplete
                fullWidth
                multiple
                options={issues.data.getIssuesOfOrganization.map((_issue) => _issue.id) || []}
                getOptionLabel={(option) => issues.data.getIssuesOfOrganization.find(
                  (_issue) => _issue.id === option,
                )?.name || ''}
                value={issue.issueLinks || []}
                onChange={(e, value) => {
                  setIssue({
                    ...issue,
                    issueLinks: value
                      .filter((_value) => issues.data
                        .getIssuesOfOrganization.map((_issue) => _issue.id)
                        .includes(_value)),
                  });
                  setIsChanged(true);
                }}
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    color="secondary"
                  />
                )}
              />
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Приоритет решения*
              </span>
              <Select
                error={checkEmpty(issue.priority)}
                variant="standard"
                color="secondary"
                value={issue.priority}
                onChange={(e) => {
                  setIssue({ ...issue, priority: e.target.value });
                  setIsChanged(true);
                }}
              >
                {priorities.map((priority, key) => (
                  <MenuItem key={key} value={key}>{priorityShield(key)}</MenuItem>
                ))}
              </Select>
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Исполнитель
              </span>
              <Autocomplete
                fullWidth
                options={users.data.getUsersOfOrganization.map((user) => user.id) || []}
                getOptionLabel={(option) => users.data.getUsersOfOrganization.find(
                  (user) => user.id === option,
                )?.name || ''}
                value={issue.executorUserId || ''}
                onChange={(e, value) => {
                  setIssue({ ...issue, executorUserId: value });
                  setIsChanged(true);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    color="secondary"
                  />
                )}
              />
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Домен*
              </span>
              <Select
                error={checkEmpty(issue.domainId)}
                variant="standard"
                color="secondary"
                value={issue.domainId}
                onChange={(e) => {
                  setIssue({
                    ...issue, domainId: e.target.value, issueTypeId: null, stageId: null,
                  });
                  setIsChanged(true);
                  stages.refetch({ getDomainId: e.target.value });
                }}
              >
                {domains.data?.getDomainsOfOrganization?.map((domain) => (
                  <MenuItem key={domain.id} value={domain.id}>
                    {domain.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Тип задачи
              </span>
              <Select
                variant="standard"
                color="secondary"
                value={issue.issueTypeId}
                onChange={(e) => {
                  setIssue({ ...issue, issueTypeId: e.target.value, stageId: null });
                  setIsChanged(true);
                }}
              >
                <MenuItem value={null}>
                  Без типа
                </MenuItem>
                {stages.data?.getDomain?.issueTypes.map((issueType) => (
                  <MenuItem key={issueType.id} value={issueType.id}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      {issueType.isStart ? <PlayArrow /> : null}
                      {issueType.isEnd ? <Lock /> : null}
                      {issueType.name}
                    </div>
                  </MenuItem>
                ))}
              </Select>
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Этап*
              </span>
              <Select
                error={checkEmpty(issue.stageId)}
                variant="standard"
                color="secondary"
                value={issue.stageId}
                onChange={(e) => {
                  setIssue({ ...issue, stageId: e.target.value });
                  setIsChanged(true);
                }}
              >
                {filteredStages.map((stage) => (
                  <MenuItem key={stage.id} value={stage.id}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      {stage.isStart ? <PlayArrow /> : null}
                      {stage.isEnd ? <Lock /> : null}
                      {stage.name}
                    </div>
                  </MenuItem>
                ))}
              </Select>
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Потребности
              </span>
              <Select
                value={issue.driverIds || []}
                onChange={(e) => {
                  setIssue({
                    ...issue,
                    driverIds: e.target.value
                      .filter((value) => drivers.data
                        .getDriversOfOrganization.map((_driver) => _driver.id)
                        .includes(value)),
                  });
                  setIsChanged(true);
                }}
                variant="standard"
                color="secondary"
                multiple
              >
                {drivers.data.getDriversOfOrganization.map((currentDriver) => (
                  <MenuItem key={currentDriver.id} value={currentDriver.id}>
                    {currentDriver.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
            <div>
              <span style={styles.fieldTitle}>
                Дедлайн
              </span>
              <TextField
                type="date"
                value={issue.deadlineAt ? moment(issue.deadlineAt).format('YYYY-MM-DD') : ''}
                onChange={(e) => {
                  setIssue({
                    ...issue,
                    deadlineAt: e.target.value,
                  });
                  setIsChanged(true);
                }}
                variant="standard"
                color="secondary"
              />
              <IconButton onClick={() => setIssue({ ...issue, deadlineAt: null })} size="small">
                <Clear />
              </IconButton>
            </div>
          </Box>
        </div>
        <span style={styles.fieldTitle}>
          Описание
        </span>
        <Box style={{
          border: '1px solid #ddd',
          borderRadius: '5px',
          padding: '5px',
          marginBottom: '5px',
        }}
        >
          <MarkdownEditorLoaded
            value={issue.description}
            onChange={(e) => {
              setIssue({ ...issue, description: e });
              setIsChanged(true);
            }}
          />
        </Box>
        <div style={{ marginTop: '40px', textAlign: 'right' }}>
          <Button
            variant="outlined"
            color="tertiary"
            style={{ marginRight: '30px' }}
            onClick={() => close()}
          >
            Закрыть

          </Button>
          <Button
            variant="contained"
            disabled={!canSubmit}
            onClick={async () => {
              if (issueId) {
                await edit({
                  variables: {
                    id: issueId,
                    issue: { ...issue, authorUserId: undefined },
                  },
                });
              } else {
                await save({ variables: { issue: { ...issue, authorUserId: undefined } } });
              }
              if (props.isDialog) {
                props.onClose();
                props.refetch();
              } else {
                router.push(`${router.asPath}/../`);
              }
            }}
          >
            {isNew() ? 'Создать' : 'Сохранить'}
          </Button>
        </div>
      </Box>
      <SimpleDialog
        open={closeDialog}
        title="Сохранить изменения перед закрытием?"
        actions={[
          {
            text: 'Не закрывать',
            onClick: () => {
              setCloseDialog(false);
            },
          },
          {
            text: 'Закрыть',
            onClick: () => {
              setCloseDialog(false);
              if (props.isDialog) {
                props.onClose();
              } else {
                router.push(`${router.asPath}/../`);
              }
            },
          },
          ...(canSubmit ? [{
            text: 'Сохранить',
            onClick: async () => {
              if (issueId) {
                await edit({
                  variables: {
                    id: issueId,
                    issue: {
                      ...issue,
                      authorUserId: undefined,
                    },
                  },
                });
              } else {
                await save({ variables: { issue: { ...issue, authorUserId: undefined } } });
              }
              if (props.isDialog) {
                props.onClose();
                props.refetch();
              } else {
                router.push(`${router.asPath}/../`);
              }
            },
          }] : []),
        ]}
      />
      <SimpleDialog
        open={deleteDialog}
        title="Удаление задачи"
        text="Вы уверены, что хотите удалить эту задачу?"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          if (issueId) {
            await remove({ variables: { id: issueId } });
          } else {
            await save({ variables: { issue } });
          }
          if (props.isDialog) {
            props.onClose();
            props.refetch();
          } else {
            router.push(`${router.asPath}/../../`);
          }
        }}
      />
    </div>
  );

  if (props.isDialog) {
    return (
      <Dialog
        open={props.open}
        onClose={() => {
          close();
        }}
        maxWidth="lg"
        fullWidth
      >
        <DialogContent>
          {result}
        </DialogContent>
      </Dialog>
    );
  }

  return result;
}
