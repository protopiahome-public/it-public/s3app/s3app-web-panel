import { useState, useMemo } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { gql, useMutation, useQuery } from '@apollo/client';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import {
  Button, Box, MenuItem, ListItemIcon,
  Tooltip,
} from '@mui/material';
import {
  Delete, Edit, OpenInBrowser,
} from '@mui/icons-material';
import {
  MaterialReactTable,
  useMaterialReactTable,
  MRT_GlobalFilterTextField as GlobalFilterTextField,
  MRT_TablePagination as TablePagination,
  MRT_ToggleFiltersButton as ToggleFiltersButton,
} from 'material-react-table';
import { MRT_Localization_RU as LocalizationRU } from 'material-react-table/locales/ru';

import Link from 'next/link';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import TableDateRangeFilter from '../../../../components/TableDateRangeFilter';
import MarkdownPreviewLoaded from '../../../../components/MarkdownPreviewLoaded';

const DELETE_THOUGHT = gql`
  mutation Mutation($id: ID!) {
    deleteThought(id: $id)
  }
`;

const GET_DOMAIN = gql`
  query getDomain($id: ID!, $withSubDomains: Boolean) {
    getDomain(id: $id) {
      thoughts(withSubDomains: $withSubDomains){
        id
        name
        description
        authorUser {
          id
          name
        }
        domain {
          id
          name
        }
        createdAt
      }
    }
  }
`;

function Thoughts() {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();

  const { data, refetch, loading } = useQuery(GET_DOMAIN, {
    variables: {
      id: router.query.id,
      withSubDomains: true,
    },
  });

  const [deleteThought] = useMutation(DELETE_THOUGHT);
  const [deleteDialog, setDeleteDialog] = useState({ open: false, thought: {} });

  const columns = useMemo(() => [
    {
      accessorFn: (row) => new Date(row.createdAt),
      header: 'Дата создания',
      // eslint-disable-next-line react/no-unstable-nested-components
      Header: ({ column }) => (
        <Tooltip title={column.columnDef.header}>
          {column.columnDef.header}
        </Tooltip>
      ),
      sortingFn: 'datetime',
      filterFn: (row, _id, filterValue) => {
        const date = new Date(row.original.createdAt);
        if (!filterValue.length) return true;
        return filterValue[0] <= date && date <= filterValue[1];
      },
      Cell: ({ cell }) => moment(cell.getValue()).format('DD.MM.YYYY'),
      // eslint-disable-next-line react/no-unstable-nested-components
      Filter: ({ column }) => <TableDateRangeFilter column={column} />,
      maxSize: 100,
      grow: false,
    },
    {
      accessorKey: 'name',
      header: 'Название',
      filterFn: 'contains',
      filterVariant: 'text',
      muiTableBodyCellProps: (cell) => ({
        className: 'cursor-pointer text-wrap',
        onClick: () => router.push(`/domains/${cell.row.original.domain.id}/thoughts/${cell.row.original.id}`),
      }),
    },
    {
      accessorKey: 'domain.name',
      filterVariant: 'autocomplete',
      header: 'Домен',
      // eslint-disable-next-line react/no-unstable-nested-components
      Cell: ({ row }) => (
        <Link
          className="text-wrap"
          href={`/domains/${row.original.domain.id}`}
        >
          {row.original.domain.name}
        </Link>
      ),
      minSize: 2,
      grow: false,
    },
    {
      accessorKey: 'authorUser.name',
      filterVariant: 'autocomplete',
      header: 'Автор',
      muiFilterTextFieldProps: {
        autoComplete: 'off',
        inputProps: {
          form: {
            autocomplete: 'off',
          },
        },
      },
      maxSize: 100,
      grow: false,
    },
  ], [router]);

  const table = useMaterialReactTable({
    columns,
    data: data?.getDomain.thoughts || [],
    state: { isLoading: loading },
    localization: LocalizationRU,
    globalFilterFn: 'contains',
    columnFilterModeOptions: ['contains'],
    initialState: {
      showColumnFilters: true,
      showGlobalFilter: true,
      pagination: { pageSize: 20, pageIndex: 0 },
    },
    displayColumnDefOptions: { 'mrt-row-actions': { minSize: 80, grow: false }, 'mrt-row-expand': { maxSize: 40, grow: false } },
    enableRowActions: true,
    layoutMode: 'grid',
    paginationDisplayMode: 'pages',
    positionActionsColumn: 'last',
    enableFacetedValues: true,
    muiTablePaperProps: {
      className: 'shadow-none grow flex flex-col',
    },
    muiTableContainerProps: {
      className: 'grow border-solid border-0 border-t border-gray-200 flex max-h-min ',
    },
    muiSearchTextFieldProps: {
      size: 'small',
      variant: 'outlined',
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiFilterTextFieldProps: {
      autoComplete: 'off',
      inputProps: {
        form: {
          autocomplete: 'off',
        },
      },
    },
    muiPaginationProps: {
      color: 'secondary',
      shape: 'rounded',
      variant: 'outlined',
    },
    renderTopToolbar: ({ table: _table }) => (
      <Box sx={{ display: 'flex', justifyContent: 'space-between', p: '8px' }}>
        <Box sx={{ display: 'flex', gap: '8px', alignItems: 'center' }}>
          <GlobalFilterTextField table={_table} />
          <ToggleFiltersButton table={_table} />
        </Box>
        <Box sx={{ display: 'flex', gap: '8px', alignItems: 'center' }}>
          <TablePagination table={_table} />
          <Button
            color="secondary"
            variant="contained"
            onClick={() => router.push(`${router.asPath}/create`)}
          >
            Добавить мысль
          </Button>
        </Box>
      </Box>
    ),
    renderRowActionMenuItems: ({ row }) => [
      <MenuItem
        key="open"
        onClick={() => router.push(`/domains/${row.original.domain.id}/thoughts/${row.original.id}`)}
      >
        <ListItemIcon><OpenInBrowser fontSize="small" /></ListItemIcon>
        Открыть
      </MenuItem>,
      <MenuItem
        key="edit"
        onClick={() => router.push(`/domains/${row.original.domain.id}/thoughts/${row.original.id}/edit`)}
      >
        <ListItemIcon><Edit fontSize="small" /></ListItemIcon>
        Редактировать
      </MenuItem>,
      <MenuItem
        key="delete"
        onClick={() => setDeleteDialog({ open: true, thought: row.original })}
      >
        <ListItemIcon><Delete fontSize="small" /></ListItemIcon>
        Удалить
      </MenuItem>,
    ],
    renderDetailPanel: ({ row }) => (
      <div className="flex flex-col">
        <div>
          <MarkdownPreviewLoaded value={row.original.description || 'Нет описания'} />
        </div>
      </div>
    ),
  });

  return (
    <>
      <Head>
        <title>Мысли и идеи</title>
      </Head>
      <Box sx={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="thoughts" />
        </LeftSubPanel>
        <Box sx={{ flexGrow: 1 }}>
          <MaterialReactTable table={table} />
        </Box>
      </Box>
      <SimpleDialog
        open={deleteDialog.open}
        title="Удаление мысли"
        text={`Вы уверены, что хотите удалить мысль "${deleteDialog.thought.name}"?`}
        onCancel={() => setDeleteDialog({ ...deleteDialog, open: false })}
        onAccept={async () => {
          try {
            await deleteThought({
              variables: { id: deleteDialog.thought.id },
              onCompleted: () => {
                enqueueSnackbar('Мысль успешно удалена', { variant: 'success' });
                setDeleteDialog({ ...deleteDialog, open: false });
                refetch();
              },
            });
          } catch (e) {
            enqueueSnackbar(e.message, { variant: 'error' });
          }
        }}
      />
    </>
  );
}

export default Thoughts;
