import { gql, useQuery } from '@apollo/client';
import Head from 'next/head';
import { useRouter } from 'next/router';

import {
  Box, Typography, useTheme,
} from '@mui/material';

import DomainLeftPanel from '../../../components/DomainLeftPanel';
import LeftSubPanel from '../../../components/LeftSubPanel';
import S3Avatar from '../../../components/S3Avatar';

import Process from '../../../assets/process.svg';
import Edit from '../../../assets/edit.svg';
import MarkdownPreviewLoaded from '../../../components/MarkdownPreviewLoaded';

const GET_DOMAIN = gql`
query getDomain($id: ID!) {
    getDomain(id: $id) {
      name
      description
      performanceCriteria
      mainDriver {
        description
      }
      implementationStrategy
      restrictions
      participants {
        id
        name
      }
      subDomains {
        name
        domainType
        participants {
          id
          name
        }
      }
      stages {
          id
          name
      }
    }
  }
  `;

function Domain() {
  const router = useRouter();
  const theme = useTheme();

  const style = {
    fieldName: {
      fontWeight: 500,
      width: '230px',
      fontSize: '14px',
      lineHeight: '17px',
      color: '#A2BDE4',
    },
    roleHeader:
      {
        width: '212px',
        height: '30px',
        background: theme.palette.secondary.main,
        color: '#4B5A73',
        border: '1px solid',
        borderColor: theme.palette.secondary.main,
        textAlign: 'center',
      },
    roleCell: {
      width: '212px',
      height: '40px',
      border: '1px solid',
      borderColor: theme.palette.secondary.main,
      textAlign: 'center',
    },
    fieldContent: {
      width: '430px',
      height: '30px',
      border: '2px solid',
      borderColor: theme.palette.secondary.main,
      borderRadius: '5px',
      fontSize: '14px',
      display: 'flex',
      alignItems: 'center',
      padding: '10px',
      boxSizing: 'border-box',
    },
  };

  const { id } = router.query;
  const { loading, data } = useQuery(GET_DOMAIN, { variables: { id } });

  if (loading) {
    return null;
  }

  return (
    <>
      <Head>
        <title>Домен</title>
      </Head>
      <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="domain" />
        </LeftSubPanel>

        <Box sx={{
          mt: 4.5,
          flex: 1,
          display: 'flex',
          flexDirection: 'column',
          ml: 4.5,
        }}
        >
          <Box display="inline-flex" gap={16} mb={5}>
            <Box sx={{
              bgcolor: 'tertiary.main',
              borderRadius: '5px',
              height: 100,
              width: 100,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
            >
              <Typography color="white" variant="h3">
                {data?.getDomain.name?.toUpperCase()
                  .split(' ')
                  .slice(0, 2)
                  .map((word) => word.charAt(0))
                  .join('')}
              </Typography>
            </Box>
            <Box>
              <Box display="flex" gap={1}>
                <Typography lineHeight={1} color="black" variant="h6">{data?.getDomain.name}</Typography>
                <Edit onClick={() => router.push(`${router.asPath}/edit`)} style={{ stroke: theme.palette.primary.main, cursor: 'pointer' }} />
              </Box>
              <Typography width={580} mt={1} variant="body2">{data?.getDomain.description}</Typography>
            </Box>
          </Box>

          <Box display="inline-flex">
            <Box sx={style.fieldName}>Основной драйвер:</Box>
            <Box>
              <Box>
                <MarkdownPreviewLoaded value={data?.getDomain.mainDriver?.description || 'Драйвера нет'} />
              </Box>
            </Box>

          </Box>
          <Box display="inline-flex" my={8}>
            <Box sx={style.fieldName}>Участники домена:</Box>
            <Box>
              <table style={{ borderCollapse: 'collapse', width: '100%' }}>
                <thead>
                  <tr>
                    <td style={style.roleHeader}>Участник</td>
                    <td style={style.roleHeader}>Роль</td>
                  </tr>
                </thead>
                <tbody>
                  {data.getDomain.participants.length > 0 ? (
                    <>
                      {data.getDomain.subDomains.map((subDomain) => {
                        if (subDomain.domainType !== 'role') {
                          return null;
                        }
                        return subDomain.participants.map((participant, index) => (
                          <tr key={index}>
                            <td style={style.roleCell}>
                              <Box display="flex" alignItems="center">
                                <S3Avatar name={participant.name} size={24} sx={{ mx: 1 }} />
                                <Typography variant="body2" lineHeight={1}>{participant.name}</Typography>
                              </Box>
                            </td>
                            <td style={style.roleCell}>{subDomain.name}</td>
                          </tr>
                        ));
                      })}
                      {data.getDomain.participants.map((participant, index) => (
                        <tr key={index}>
                          <td style={style.roleCell}>
                            <Box display="flex" alignItems="center">
                              <S3Avatar name={participant.name} size={24} sx={{ mx: 1 }} />
                              <Typography variant="body2" lineHeight={1}>{participant.name}</Typography>
                            </Box>
                          </td>
                          <td style={style.roleCell}>Участник домена</td>
                        </tr>
                      ))}
                    </>
                  ) : (
                    <tr>
                      <td colSpan="2" style={style.roleCell}>Ролей нет</td>
                    </tr>
                  )}
                </tbody>
              </table>
            </Box>
          </Box>
          <Box display="inline-flex">
            <Box sx={style.fieldName}>Стратегия реализации:</Box>
            <Box>
              <Box sx={style.fieldContent}>{data.getDomain.implementationStrategy || 'Стратегии нет'}</Box>
            </Box>

          </Box>
          <Box display="inline-flex" my={1}>
            <Box sx={style.fieldName}>Критерии эффективности:</Box>
            <Box>
              <Box sx={style.fieldContent}>{data.getDomain.performanceCriteria || 'Критериев нет'}</Box>
            </Box>

          </Box>
          <Box display="inline-flex">
            <Box sx={style.fieldName}>Ограничения домена:</Box>
            <Box>
              <Box sx={style.fieldContent}>{data.getDomain.restrictions || 'Ограничений нет'}</Box>
            </Box>
          </Box>
        </Box>

        <Box sx={{
          width: 256, bgcolor: 'secondary.main', display: 'flex', flexDirection: 'column', alignItems: 'center',
        }}
        >
          <Box m={2}>Цепочка создания ценности</Box>
          {/* <Box sx={{
            width: 208,
            height: 52,
            backgroundColor: '#FFFFFF',
            borderRadius: '5px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 1,
          }}
          >
            {data.getDomain.valueChain?.resources}
          </Box> */}
          <Box>
            {data.getDomain.stages?.map((stage, index) => (
              <Box
                key={index}
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  position: 'relative',
                  marginBottom: 8,
                }}
              >
                <Process />
                <Box style={{ position: 'absolute' }}>{stage.name}</Box>
              </Box>
            ))}
          </Box>
          {/* <Box style={{
            width: 208,
            height: 40,
            backgroundColor: '#FFFFFF',
            borderRadius: 5,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
          >
            {data.getDomain.valueChain?.products}

          </Box> */}
        </Box>
      </Box>
    </>
  );
}

export default Domain;
