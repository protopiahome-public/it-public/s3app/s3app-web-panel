import Head from 'next/head';
import {
  Box,
  Button,
  MenuItem,
  Select,
  TextField,
  useTheme,
  IconButton,
} from '@mui/material';
import { useQuery, gql, useMutation } from '@apollo/client';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { checkEmpty, proposalStatuses } from '../../../../components/common';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import DomainLeftPanel from '../../../../components/DomainLeftPanel';

import Delete from '../../../../assets/delete.svg';
import Close from '../../../../assets/close.svg';
import SimpleDialog from '../../../../components/SimpleDialog';
import MarkdownEditorLoaded from '../../../../components/MarkdownEditorLoaded';

const GET_DRIVERS = gql`
  query getDriversOfOrganization($domainId: ID!) {
    getDriversOfOrganization(domainId: $domainId) {
      id 
      name
    }
  }
`;

const CREATE_PROPOSAL = gql`
  mutation createProposal($proposal: ProposalInput!) {
    createProposal(proposal: $proposal) {
      id
    }
  }
`;

const EDIT_PROPOSAL = gql`
  mutation editProposal($id: ID!, $proposal: ProposalInput!) {
    editProposal(id: $id, proposal: $proposal) {
      id
    }
  }
`;

const DELETE_PROPOSAL = gql`
  mutation deleteProposal($id: ID!) {
    deleteProposal(id: $id)
  }
`;

const GET_PROPOSAL = gql`
  query getProposal($id: ID!) {
    getProposal(id: $id) {
      id
      name
      description
      status
      drivers {
        id
        name
      }
      createdAt
      authorUser {
        id
        name
      }
      domain {
        name
        id
      }
    }
  }
`;

const GET_DOMAINS = gql`
  query getDomainsOfOrganization($domainId: ID!) {
    getDomainsOfOrganization(domainId: $domainId) {
      id
      name
    }
  }
`;

const styles = {
  fields: {
    '&': { width: '50%', paddingTop: '10px' },
    '& > div': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  description: {
    border: '2px solid #A2BDE4',
    borderRadius: '5px',
    padding: '20px',
  },
  image: {
    marginLeft: '4px',
    borderRadius: '50%',
    borderWidth: '2px',
    borderStyle: 'solid',
    borderColor: (theme) => theme.palette.primary.main,
  },
};

export default function Proposal() {
  const theme = useTheme();

  const router = useRouter();

  const isNew = () => !router.query['proposal-id'];

  const domainId = router.query.id;
  const drivers = useQuery(GET_DRIVERS, { variables: { domainId } });

  const domains = useQuery(GET_DOMAINS, { variables: { domainId } });

  const [save] = useMutation(CREATE_PROPOSAL);
  const [edit] = useMutation(EDIT_PROPOSAL);
  const [remove] = useMutation(DELETE_PROPOSAL);

  const [proposal, setProposal] = useState({
    name: '',
    description: '',
    domainId: router.query.id,
    status: 'new',
  });

  useQuery(GET_PROPOSAL, {
    variables: { id: router.query['proposal-id'] },
    skip: !router.query['proposal-id'],
    onCompleted: (data) => {
      setProposal({
        name: data.getProposal.name,
        description: data.getProposal.description,
        driverIds: data.getProposal.drivers?.map((driver) => driver.id) || [],
        domainId: data.getProposal.domain?.id,
        authorUserId: data.getProposal.author?.id,
        status: data.getProposal.status,
      });
    },
  });

  const [deleteDialog, setDeleteDialog] = useState(false);
  const [closeDialog, setCloseDialog] = useState(false);

  if (drivers.loading) {
    return null;
  }

  return (
    <div style={{ display: 'flex' }}>
      <Head>
        <title>
          {router.query['proposal-id'] ? `Предложение ${proposal.name}` : 'Новое предложени'}
        </title>
      </Head>
      <LeftSubPanel title="Домены">
        <DomainLeftPanel selected="proposals" />
      </LeftSubPanel>
      <Box sx={{
        flex: 1, mx: 4, display: 'flex', flexDirection: 'column',
      }}
      >
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Box
            style={{
              display: 'flex',
              alignItems: 'center',
              width: '100%',
              borderBottom: `${theme.palette.primary.main} solid 2px`,
            }}
          >
            <span style={styles.fieldTitle}>Название*</span>
            <TextField
              value={proposal.name}
              error={checkEmpty(proposal.name)}
              size="normal"
              variant="standard"
              color="secondary"
              fullWidth
              sx={checkEmpty(proposal.name) ? undefined : {
                '& .MuiInput-underline::before': {
                  display: 'none',
                },
                '& .MuiInput-underline::after': {
                  display: 'none',
                },
              }}
              onChange={(e) => {
                setProposal({
                  ...proposal,
                  name: e.target.value,
                });
              }}
            />

            {!isNew()
                && (
                <IconButton onClick={() => setDeleteDialog(true)}>
                  <Delete
                    style={{ stroke: theme.palette.primary.main }}
                  />
                </IconButton>
                )}

            <IconButton onClick={() => {
              if (isNew()) router.back();
              else setCloseDialog(true);
            }}
            >
              <Close
                style={{ stroke: theme.palette.primary.main }}
              />
            </IconButton>
          </Box>
        </Box>
        <div style={{ display: 'flex', marginBottom: '20px' }}>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Создатель
              </span>
              <span style={styles.fieldValue}>
                {proposal.authorUserId}
              </span>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Статус
              </span>
              <Select
                value={proposal.status}
                onChange={(e) => {
                  setProposal({
                    ...proposal,
                    status: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {Object.keys(proposalStatuses).map((status) => (
                  <MenuItem key={status} value={status}>
                    {proposalStatuses[status].label}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Потребности
              </span>
              <Select
                value={proposal.driverIds || []}
                onChange={(e) => {
                  setProposal({
                    ...proposal,
                    driverIds: e.target.value
                      .filter((value) => drivers.data
                        .getDriversOfOrganization.map((_driver) => _driver.id)
                        .includes(value)),
                  });
                }}
                variant="standard"
                color="secondary"
                multiple
              >
                {drivers.data.getDriversOfOrganization.map((currentDriver) => (
                  <MenuItem key={currentDriver.id} value={currentDriver.id}>
                    {currentDriver.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
          <Box sx={styles.fields}>
            <div>
              <span style={styles.fieldTitle}>
                Домен
              </span>
              <Select
                value={proposal.domainId}
                onChange={(e) => {
                  setProposal({
                    ...proposal,
                    domainId: e.target.value,
                  });
                }}
                variant="standard"
                color="secondary"
              >
                {domains.data?.getDomainsOfOrganization?.map((domain) => (
                  <MenuItem key={domain.id} value={domain.id}>
                    {domain.name}
                  </MenuItem>
                ))}
              </Select>
            </div>
          </Box>
        </div>
        <Box style={{
          border: '1px solid #ddd',
          borderRadius: '5px',
          padding: '5px',
          marginBottom: '5px',
        }}
        >
          <MarkdownEditorLoaded
            value={proposal.description}
            onChange={(e) => setProposal({ ...proposal, description: e })}
          />
        </Box>
        <div style={{ marginTop: '40px', textAlign: 'right' }}>
          <Button
            variant="outlined"
            color="tertiary"
            style={{ marginRight: '30px' }}
            onClick={() => setCloseDialog(true)}
          >
            Закрыть

          </Button>
          <Button
            variant="contained"
            disabled={
              checkEmpty(proposal.name) || checkEmpty(proposal.domainId)
            }
            onClick={async () => {
              if (router.query['proposal-id']) {
                await edit({ variables: { id: router.query['proposal-id'], proposal: { ...proposal, authorUserId: undefined } } });
              } else {
                await save({ variables: { proposal: { ...proposal, authorUserId: undefined } } });
              }
              router.push(`${router.asPath}/../`);
            }}
          >
            {isNew() ? 'Создать' : 'Сохранить'}
          </Button>
        </div>
      </Box>
      <SimpleDialog
        open={closeDialog}
        title="Сохранить изменения перед закрытием?"
        onCancel={() => { setCloseDialog(false); router.push(`${router.asPath}/../`); }}
        onAccept={async () => {
          if (router.query['proposal-id']) {
            await edit({ variables: { id: router.query['proposal-id'], proposal: { ...proposal, authorUserId: undefined } } });
          } else {
            await save({ variables: { proposal: { ...proposal, authorUserId: undefined } } });
          }
          router.push(`${router.asPath}/../`);
        }}
      />
      <SimpleDialog
        open={deleteDialog}
        title="Удаление предложения"
        text="Вы уверены, что хотите удалить это предложение?"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          if (router.query['proposal-id']) {
            await remove({ variables: { id: router.query['proposal-id'] } });
          } else {
            await save({ variables: { proposal } });
          }
          router.push(`${router.asPath}/../../`);
        }}
      />
    </div>
  );
}
