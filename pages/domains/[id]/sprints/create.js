import Head from 'next/head';
import Link from 'next/link';
import { useState } from 'react';
import { useRouter } from 'next/router';
import { gql, useMutation, useQuery } from '@apollo/client';
import {
  Autocomplete,
  Box,
  Button,
  IconButton,
  MenuItem,
  Select,
  Table,
  TableCell,
  TableRow,
  TextField,
  Tooltip,
  useTheme,
} from '@mui/material';

import {
  checkEmpty,
  priorities,
  priorityShield,
} from '../../../../components/common';

import DomainLeftPanel from '../../../../components/DomainLeftPanel';
import LeftSubPanel from '../../../../components/LeftSubPanel';
import SimpleDialog from '../../../../components/SimpleDialog';
import RangeDatePicker from '../../../../components/RangeDatePicker';

import Delete from '../../../../assets/delete.svg';
import Close from '../../../../assets/close.svg';
import S3Avatar from '../../../../components/S3Avatar';
import MarkdownEditorLoaded from '../../../../components/MarkdownEditorLoaded';

const GET_DOMAINS = gql`
query getDomainsOfOrganization($domainId: ID!) {
  getDomainsOfOrganization(domainId: $domainId) {
    id
    name
    participants {
      id
      name
    }
  }
}
`;

const GET_DOMAIN = gql`
query GetDomain($getDomainId: ID!) {
  getDomain(id: $getDomainId) {
    participants {
      id
      name
    }
  }
}
`;

const GET_SPRINT = gql`
query getSprint($id: ID!) {
  getSprint(id: $id) {
    id
    name
    startDate
    endDate
    goal
    participants {
      id
      name      
    }
    issues {
      id
      name
      priority
      executor {
        id
        name
      }
    }
    domain {
      id
      name
    }
  }
}
  `;

const GET_ISSUES = gql`
  query getIssuesOfOrganization($domainId: ID!) {
    getIssuesOfOrganization(domainId: $domainId) {
      id 
      name
      priority
      executor {
        id
        name
      }
    }
  }
`;

const EDIT_SPRINT = gql`
  mutation editSprint($id: ID!, $sprint: SprintUpdateInput!) {
    editSprint(id: $id, sprint: $sprint) {
      id
    }
  }
`;

const CREATE_SPRINT = gql`
mutation CreateSprint($sprint: SprintInput!) {
  createSprint(sprint: $sprint) {
    id
  }
}
`;

const DELETE_SPRINT = gql`
mutation Mutation($id: ID!) {
  deleteSprint(id: $id)
}
`;

const styles = {
  fields: {
    '&': { width: '50%', paddingTop: '10px' },
    '& > Box': {
      padding: '10px 0px',
      display: 'flex',
      alignItems: 'center',
      height: '20px',
    },
  },
  fieldTitle: {
    color: '#A2BDE4',
    paddingRight: 4,
    fontSize: '14px',
  },
  fieldValue: {
    fontSize: '14px',
  },
  border: {
    border: '2px solid #DAE5F4',
    borderRadius: '5px',
  },
  header: { fontSize: '14px', color: '#4B5A73' },
};

function SprintEdit() {
  const theme = useTheme();
  const router = useRouter();

  const [edit] = useMutation(EDIT_SPRINT);
  const [create] = useMutation(CREATE_SPRINT);
  const [deleteSprint] = useMutation(DELETE_SPRINT);

  const [selectedIssue, setSelectedIssue] = useState(null);

  const isNew = () => !router.query['sprint-id'];

  const [sprint, setSprint] = useState({
    name: '',
    issues: [],
    goal: '',
    domainId: router.query.id,
    startDate: new Date(),
    endDate: new Date(),
    participants: [],
  });

  const issues = useQuery(GET_ISSUES, {
    variables: { domainId: router.query.id },
  });
  const domains = useQuery(GET_DOMAINS, { variables: { domainId: router.query.id } });

  const domain = useQuery(
    GET_DOMAIN,
    {
      variables: { getDomainId: router.query.id },
      onCompleted: isNew() ? ((data) => setSprint(
        {
          ...sprint,
          participants: data.getDomain.participants.map((item) => item.id),
        },
      )) : undefined,
    },
  );

  const handleUserSelect = (e) => setSprint({ ...sprint, participants: e.target.value });

  const { loading, data } = useQuery(GET_SPRINT, {
    variables: { id: router.query['sprint-id'] },
    skip: isNew(),
    onCompleted: (loadedData) => {
      setSprint({
        domainId: loadedData.getSprint.domain.id,
        name: loadedData.getSprint.name,
        issues: loadedData.getSprint.issues.map((issue) => issue.id),
        goal: loadedData.getSprint.goal,
        startDate: loadedData.getSprint.startDate,
        endDate: loadedData.getSprint.endDate,
        participants: loadedData.getSprint.participants.map((item) => item.id),
      });
    },
  });

  const [deleteDialog, setDeleteDialog] = useState(false);
  const [closeDialog, setCloseDialog] = useState(false);

  if (loading || issues.loading || domain.loading || domains.loading) {
    return null;
  }

  return (
    <>
      <Head>
        <title>Новый спринт</title>
      </Head>
      <Box sx={{ display: 'flex' }}>
        <LeftSubPanel title="Домены">
          <DomainLeftPanel selected="sprints" />
        </LeftSubPanel>

        <Box sx={{
          flex: 1, ml: 4, display: 'flex', flexDirection: 'column',
        }}
        >
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Box
              style={{
                display: 'flex',
                alignItems: 'center',
                width: '100%',
                borderBottom: `${theme.palette.primary.main} solid 2px`,
              }}
            >
              <span style={styles.fieldTitle}>Название*</span>
              <TextField
                error={checkEmpty(sprint.name)}
                value={sprint.name}
                size="normal"
                variant="standard"
                color="secondary"
                fullWidth
                sx={checkEmpty(sprint.name) ? undefined : {
                  '& .MuiInput-underline::before': {
                    display: 'none',
                  },
                  '& .MuiInput-underline::after': {
                    display: 'none',
                  },
                }}
                onChange={(e) => {
                  setSprint({
                    ...sprint,
                    name: e.target.value,
                  });
                }}
              />

              {!isNew()
                && (
                <IconButton onClick={() => setDeleteDialog(true)}>
                  <Delete
                    style={{ stroke: theme.palette.primary.main }}
                  />
                </IconButton>
                )}

              <IconButton onClick={() => setCloseDialog(true)}>
                <Close
                  style={{ stroke: theme.palette.primary.main }}
                />
              </IconButton>
            </Box>
          </Box>

          <Box sx={{ display: 'flex', flex: 1 }}>
            <Box sx={{ flex: 1 }}>
              <Box sx={{ mr: 4 }}>
                <Box sx={{
                  display: 'flex',
                  mt: 2.75,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  px: 1,
                  fontSize: 14,
                }}
                >
                  <Box>
                    <span style={styles.fieldTitle}>ID</span>
                    <span>{data?.getSprint.id || '-'}</span>
                  </Box>
                  <Box>
                    <span style={styles.fieldTitle}>Тип мероприятия</span>
                    <span style={styles.fieldValue}>Спринт</span>
                  </Box>
                  <Box sx={{ display: 'flex', alignItems: 'center' }}>
                    <span style={styles.fieldTitle}>Участники</span>
                    <Select
                      multiple
                      displayEmpty
                      variant="standard"
                      value={sprint.participants || []}
                      sx={{ minWidth: 120, maxWidth: 250 }}
                      onChange={handleUserSelect}
                      renderValue={(selected) => {
                        if (selected.length === 0) {
                          return <span>Участников нет</span>;
                        }
                        return selected.map((p) => domain.data?.getDomain.participants.find((user) => user.id === p)?.name).join(', ');
                      }}
                    >
                      {domain.data?.getDomain.participants?.map((participant) => (
                        <MenuItem
                          key={participant.id}
                          value={participant.id}
                        >
                          {participant.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </Box>
                </Box>
                <Box sx={styles.fields}>
                  <div>
                    <span style={styles.fieldTitle}>
                      Домен
                    </span>
                    <Select
                      value={sprint.domainId}
                      onChange={async (e) => {
                        await domain.refetch({ getDomainId: e.target.value });
                        setSprint({
                          ...sprint,
                          domainId: e.target.value,
                        });
                      }}
                      variant="standard"
                      color="secondary"
                    >
                      {domains.data?.getDomainsOfOrganization?.map((_domain) => (
                        <MenuItem key={_domain.id} value={_domain.id}>
                          {_domain.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </div>
                </Box>
                <Box sx={{ ...styles.header, padding: 1 }}>
                  Цель
                </Box>
                <Box style={{
                  border: '1px solid #ddd',
                  borderRadius: '5px',
                  padding: '5px',
                  marginBottom: '5px',
                }}
                >
                  <MarkdownEditorLoaded
                    value={sprint.goal}
                    onChange={(e) => setSprint({ ...sprint, goal: e })}
                  />
                </Box>

                <Box sx={{
                  padding: 1,
                  my: 2,
                }}
                >
                  <span style={styles.header}>Проблемы</span>
                  <Table size="small" className="issuesTable" sx={{ mt: 1.25, mb: 2.875 }}>
                    {sprint.issues.length > 0
                      ? (
                        <>
                          {issues.data.getIssuesOfOrganization
                            .filter((issue) => sprint.issues.includes(issue.id))
                            .map((issue) => (
                              <TableRow key={issue.id}>
                                <TableCell
                                  sx={{
                                    width: 50,
                                    height: 50,
                                    padding: 0,
                                    textAlign: 'center',
                                  }}
                                >
                                  <Tooltip title={priorities[issue.priority]?.name}>
                                    {priorityShield(issue.priority)}
                                  </Tooltip>
                                </TableCell>
                                <TableCell>
                                  <Box
                                    style={{
                                      display: 'flex',
                                      justifyContent: 'space-between',
                                      alignItems: 'center',
                                    }}
                                  >
                                    <Link href={`issues/${issue.id}`}>
                                      {issue.name}
                                    </Link>
                                    <S3Avatar name={issue.executor?.name} />
                                  </Box>
                                </TableCell>
                                <TableCell
                                  sx={{
                                    width: 50,
                                    height: 50,
                                    padding: 0,
                                    textAlign: 'center',
                                  }}
                                >
                                  <IconButton
                                    onClick={() => {
                                      setSprint({
                                        ...sprint,
                                        issues: sprint.issues.filter(
                                          (issueId) => issueId !== issue.id,
                                        ),
                                      });
                                    }}
                                  >
                                    <Delete
                                      style={{ stroke: theme.palette.primary.main }}
                                    />
                                  </IconButton>
                                </TableCell>
                              </TableRow>
                            ))}
                        </>
                      )
                      : (
                        <TableRow>
                          <TableCell
                            sx={{
                              width: 50,
                              height: 50,
                              padding: 0,
                              textAlign: 'center',
                            }}
                          >
                            Проблем нет
                          </TableCell>
                        </TableRow>
                      )}
                  </Table>
                  <Box sx={{ textAlign: 'right' }}>
                    <Autocomplete
                      value={selectedIssue}
                      size="small"
                      variant="standard"
                      sx={{ mr: 1, minWidth: '100px' }}
                      onChange={(e, value) => {
                        setSelectedIssue(value);
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="standard"
                          color="secondary"
                        />
                      )}
                      options={(issues.data.getIssuesOfOrganization || [])
                        .filter((issue) => !sprint.issues.includes(issue.id))
                        .map((issue) => issue.id)}
                      getOptionLabel={(option) => issues.data.getIssuesOfOrganization.find(
                        (issue) => issue.id === option,
                      )?.name}
                      filterSelectedOptions
                    />
                    <Button
                      variant="outlined"
                      color="primary"
                      onClick={() => {
                        if (
                          selectedIssue
                            && !sprint.issues.includes(selectedIssue)
                        ) {
                          setSprint({
                            ...sprint,
                            issues: [...sprint.issues, selectedIssue],
                          });
                        }
                      }}
                    >
                      Добавить проблему
                    </Button>
                  </Box>
                </Box>

                <Box sx={{ textAlign: 'right', p: 1 }}>
                  <Button
                    variant="contained"
                    disabled={checkEmpty(sprint.name)}
                    onClick={async () => {
                      if (isNew()) {
                        await create({ variables: { sprint } });
                      } else {
                        await edit({ variables: { id: router.query['sprint-id'], sprint } });
                      }
                      router.push(`${router.asPath}/../`);
                    }}
                  >
                    {isNew() ? 'Создать' : 'Сохранить'}
                  </Button>
                </Box>
              </Box>
            </Box>

            <Box
              sx={{
                width: 350,
                backgroundColor: '#DAE5F4',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Box sx={{ ...styles.header, my: 2.75 }}>Дата начала и окончания спринта</Box>
              <RangeDatePicker
                startDate={new Date(sprint.startDate)}
                endDate={new Date(sprint.endDate)}
                onChange={(startDate, endDate) => {
                  setSprint({
                    ...sprint,
                    startDate,
                    endDate,
                  });
                }}
              />
            </Box>
          </Box>
        </Box>

      </Box>

      <SimpleDialog
        open={deleteDialog}
        title="Удаление спринта"
        text="Вы уверены, что хотите удалить этот спринт?"
        onCancel={() => setDeleteDialog(false)}
        onAccept={async () => {
          try {
            await deleteSprint({
              variables: { id: router.query['sprint-id'] },
              onCompleted: () => {
                router.push(`${router.asPath}/../../`);
              },
            });
          } catch (e) {
            console.log(e.message);
          }
        }}
      />
      <SimpleDialog
        open={closeDialog}
        title="Выход"
        text="Сохранить изменения перед закрытием?"
        onCancel={() => router.back()}
        onAccept={async () => {
          try {
            await edit({
              variables: { id: router.query['sprint-id'], sprint },
              onCompleted: () => {
                router.push(`${router.asPath}/../../`);
              },
            });
          } catch (e) {
            console.log(e.message);
          }
        }}
      />
    </>
  );
}

export default SprintEdit;
