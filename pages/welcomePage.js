// import { useSnackbar } from 'notistack';
import { gql, useQuery /* useMutation */ } from '@apollo/client';
// import Button from '@mui/material/Button';
// import Select from '@mui/material/Select';
// import MenuItem from '@mui/material/MenuItem';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
// import InputLabel from '@mui/material/InputLabel';
// import Divider from '@mui/material/Divider';
import { useTheme } from '@mui/material/styles';
import { useRouter } from 'next/router';
// import { useState } from 'react';
import config from '../config/config';

import DomainCreator from '../components/domain-creator';

const GET_DOMAINS = gql`
query getDomains {
  getDomains {
    id
    name
    parentDomain {
      id
    }
    participants {
      id
    }
  }
}`;

// const EDIT_DOMAIN = gql`
// mutation EditDomain($editDomainId: ID!, $domain: DomainUpdateInput!) {
//   editDomain(id: $editDomainId, domain: $domain) {
//     id
//   }
// }`;
export default function WelcomePage(props) {
  const router = useRouter();
  const theme = useTheme();

  if (config.demo) router.push('/');

  // const { enqueueSnackbar } = useSnackbar();

  // const [selectedDomain, setSelectedDomain] = useState('');

  // const [editDomain] = useMutation(EDIT_DOMAIN);
  const { loading, data } = useQuery(GET_DOMAINS);
  if (loading) {
    return null;
  }

  if (!data) {
    router.push('/login');
    return null;
  }

  return (
    <Container
      maxWidth="sm"
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
      }}
    >
      <Typography component="h1" variant="h2">
        Добро пожаловать
      </Typography>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          mt: 3,
          maxWidth: '100%',
          mb: `${theme.variables.header.height}px`,
        }}
      >
        {/* <InputLabel sx={{ alignSelf: 'flex-start' }}>Список организаций</InputLabel>
        <Select
          variant="outlined"
          size="small"
          fullWidth
          sx={{ mb: 2 }}
          value={selectedDomain}
          onChange={(e) => setSelectedDomain(e.target.value)}
        >
          {data?.getDomains.map((domain) => (
            !domain.parentDomain ? (
              <MenuItem key={domain.id} value={domain.id}>
                <span>{domain.name}</span>
              </MenuItem>
            ) : ''
          ))}
        </Select>
        <Button
          variant="contained"
          fullWidth
          disabled={!selectedDomain}
          onClick={async () => {
            try {
              const participants = [];
              data.getDomains.forEach((d) => {
                if (d.id === selectedDomain) {
                  d.participants.forEach((user) => {
                    participants.push(user.id);
                  });
                }
              });

              if (!participants.includes(props.user.id)) participants.push(props.user.id);
              else {
                enqueueSnackbar(
                  'Похоже, вы уже состоите в этой организации',
                  { variant: 'warning' },
                );
                return;
              }

              await editDomain({
                variables: {
                  editDomainId: selectedDomain,
                  domain: { participants },
                },
              }).then((res) => {
                props.refetchUser();
                router.push(`/domains/${res.data.editDomain.id}`);
              });
            } catch (e) {
              console.log(e);
              enqueueSnackbar(e.message, { variant: 'error' });
              return;
            }
            enqueueSnackbar('Вы присоединились к организации', { variant: 'success' });
          }}
        >
          Присоединиться к организации
        </Button>
        <Divider sx={{ fontSize: '0.875rem', my: 2 }} flexItem variant="string">
          <b>ИЛИ</b>
        </Divider> */}
        <DomainCreator {...props} refetch={props.refetchDomain} organization />
      </Box>
    </Container>
  );
}
