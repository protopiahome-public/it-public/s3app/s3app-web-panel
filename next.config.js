// const removeImports = require('next-remove-imports')();
/** @type {import('next').NextConfig} */

module.exports = {
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });
    // eslint-disable-next-line no-param-reassign
    config.experiments = { ...config.experiments, topLevelAwait: true };
    return config;
  },
  reactStrictMode: false,
  transpilePackages: ['mui-color-input', '@mdxeditor/editor'],
  publicRuntimeConfig: {
    server: process.env.S3APP_SERVER,
    demo: process.env.S3APP_DEMO,
    license: process.env.S3APP_LICENSE,
    noConfirmation: !!process.env.S3APP_NO_CONFIRMATION,
  },
};
