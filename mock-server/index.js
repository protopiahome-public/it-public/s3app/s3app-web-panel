const { ApolloServer, gql } = require('apollo-server');
const { MockList } = require('@graphql-tools/mock');

const { DateTimeTypeDefinition } = require('graphql-scalars');

const casual = require('casual');

const typeDefs = gql`
scalar DateTime

type Query {
    me(id: ID!): User!
    getIssues: [Issue]!
    getIssue(id: ID!): Issue!
    getStages: [Stage]!
    getStageDesk: StageDesk!
    getUsers: [User]!
    getCircles: [Circle]!
    getComments: [Comment]!
    getRoles: [Role]!
    getEvents: [Event]!
    getEvent(id: ID!): Event!
    getSprints: [Sprint]!
    getSprint(id: ID!): Sprint!
    getDomains: [Domain]!
    getDomain(id: ID!): Domain!
    getStage(id: ID!): Stage!
    getCircle(id: ID!): Circle!
}

type Mutation {
    createIssue (issue: IssueInput!): Issue!
    createStage (stage: StageInput): Stage
    changeStageIssue (issueID: ID, stageID: ID): Issue
    createCircle (circle: CircleInput!): Circle!
    createComment (comment: CommentInput!): Comment!
    editComment (commentID: ID, newDescription: String): Comment ##переписать на инпут
    createRole (role: RoleInput!): Role!
    createEvent (event: EventInput!): Event!
    createSprint (sprint: SprintInput!): Sprint!
    createDomain (domain: DomainInput!): Domain!
    createUser (user: UserInput!): User!
    editUser (id: ID!, user: UserInput!): User
    deleteUser(id: ID!): Boolean
    deleteStage(id: ID!): Boolean
    editStage(id: ID!, stage: StageInput!): Stage
    deleteSprint(id: ID!): Boolean
    editSprint(id: ID!, sprint: SprintUpdateInput!): Sprint
    editRole(id: ID!, role: RoleUpdateInput!): Role
    deleteRole(id: ID!): Boolean
    editIssue(id: ID!, issue: IssueUpdateInput!): Issue
    deleteIssue(id: ID!): Boolean
    editEvent(id: ID!, event: EventUpdateInput): Event
    deleteEvent(id: ID!): Boolean
    editDomain(id: ID!, domain: DomainUpdateInput): Domain
    deleteDomain(id: ID!): Boolean
    editCircle(id: ID!, circle: CircleUpdateInput): Circle
    deleteCircle(id: ID!): Boolean
}

type User {
    id: ID
    name: String
    email: String
    comments: [Comment]
    participateInEvents: [Event]
    facilitatorInEvents: [Event]
    sprints: [Sprint]
}

input UserInput {
    name: String
    email: String
}

interface AreaOfInfluence {
    id: ID
    name: String
    isOrganization: Boolean
    domainType: String
    parentDomain: Domain
    subDomains: [Domain]
    stages: [Stage]
}

type Member {
  role: String
  user: User
}

type ValueStream {
    id: ID
    resources: String
    processes: [String]
    product: String
}

type Domain implements AreaOfInfluence{
    id: ID
    name: String
    description: String
    driver: String
    isOrganization: Boolean
    domainType: String
    parentDomain: Domain
    subDomains: [Domain]
    stages: [Stage]
    members: [Member]
    valueStream: ValueStream
    strategy: String
    criteria: String
    restrictions: String
}

input DomainInput {
    name: String!
    isOrganization: Boolean!
    domainType: String!
    parentDomain: ID
    subDomains: [ID]
    stages: [ID]!
}

input DomainUpdateInput {
    name: String
    isOrganization: Boolean
    domainType: String
    parentDomain: ID
    subDomains: [ID]
    stages: [ID]
}

type Circle implements AreaOfInfluence{
    id: ID
    name: String
    isOrganization: Boolean
    domainType: String
    parentDomain: Domain
    subDomains: [Domain]
    stages: [Stage]
}

input CircleInput {
    name: String
}

input CircleUpdateInput{
    name: String
    isOrganization: Boolean
    domainType: String
    parentDomain: ID
    subDomains: [ID]
    stages: [ID]
}

type Role implements AreaOfInfluence{
    id: ID
    name: String
    isOrganization: Boolean
    domainType: String
    parentDomain: Domain
    subDomains: [Domain]
    stages: [Stage]
}

input RoleInput{
    name: String
}

input RoleUpdateInput{
    name: String
    isOrganization: Boolean
    domainType: String
    parentDomain: ID
    subDomains: [ID]
    stages: [ID]
}

type Event{
    id: ID
    name: String
    domain: Domain
    type: String
    startDate: DateTime
    endDate: DateTime
    participants: [User]
    fasilitator: User
}

input EventInput{
    name: String!
    domain: ID!,
    type: EventType!
    startDate: DateTime!,
    endDate: DateTime!,
    participants: [ID]!,
    fasilitator: ID
}

input EventUpdateInput{
    name: String
    domain: ID
    type: EventType
    startDate: DateTime
    endDate: DateTime
    participants: [ID]
    fasilitator: ID
}

enum EventType{
    planning
    review
    retrospective
    management
    choiceForRole
}

type History{
    ## Идентификатор
    id: ID
    name: String
    description: String
    ## дата изменений
    date: DateTime
    type: String
}

type Comment{
    ## Идентификатор
    id: ID
    description: String
    author: User
    date: DateTime
}

input CommentInput{
    description: String!
    author: ID!
}

## name stage: _todo, _doing, _under_review, _done
type Stage{
    ## Идентификатор
    id: ID
    ## название
    name: String
    ## Домен которому принадлежит поток создания ценности
    parentDomain: Domain
    ## Домен с которым связан данный этап
    stageDomain: Domain
    ## список проблем связанных с этапом
    issues: [Issue]
}

input StageInput {
    domainID: ID
    name: String
}

type Sprint{
    ## Идентификатор
    id: ID
    name: String
    goal: String
    startDate: DateTime
    endDate: DateTime
    planningEvent: Event
    reviewEvent: Event
    participants: [User]
    issues: [Issue]
}

input SprintInput {
    name: String!
    goal: String!
    startDate: DateTime!
    endDate: DateTime!
    endDateForPlanningEvent: DateTime!
    endDateForReviewEvent: DateTime!
    participants: [ID]!
    issues: [ID]!
}

input SprintUpdateInput {
    name: String
    goal: String
    startDate: DateTime
    endDate: DateTime
    endDateForPlanningEvent: DateTime
    endDateForReviewEvent: DateTime
    participants: [ID]
    issues: [ID]
}

type Issue {
    ## Идентификатор
    id: ID
    ## Домен
    domain: Domain
    ## автор
    author: User
    ## название
    name: String
    ## описание
    description: String
    ## роль (компонент ПО) которую проблема затрагивает
    role: Role
    ## дата создания
    createdAt: DateTime
    ## приоритет решения
    priority: Int
    ## связь проблемы с другими проблемами
    issuesLink: [Issue]
    ## исполнитель
    executor: User
    ## Спринт в котором проблема будет решена
    sprint: Sprint
    linkedSprints: [Sprint]
    ## указывает версию ПО в которой проблема будет решена (только для ит проектов)
    ## version: String
    ## Этап на диаграмме создания потока ценности: нужно сделать, в работе (делается), на проверке, выполнено
    stage: Stage
    ## История изменений
    historyLog: [History]
    ## Комментарии к проблеме
    comments: [Comment]

    version: Int
    sprints: [Sprint]
}

input IssueInput {
    ## Домен
    domainID: ID
    ## автор
    authorID: ID
    ## название
    name: String
    ## описание
    description: String
    ## роль (компонент ПО) которую проблема затрагивает
    roleID: ID
    ## приоритет решения
    priority: Int
    ## исполнитель
    executorID: ID
    ## Спринт в котором проблема будет решена
    sprintID: ID
    ## указывает версию ПО в которой проблема будет решена (только для ит проектов)
    ## version: String
    ## Этап на диаграмме создания потока ценности: нужно сделать, в работе (делается), на проверке, выполнено
    stageID: ID
}

input IssueUpdateInput {
    domainID: ID
    authorID: ID
    name: String
    description: String
    roleID: ID
    priority: Int
    executorID: ID
    sprintID: ID
    stageID: ID
}

## Kanban
type StageDesk{
    stages: [Stage]
}

`;

const randomItem = (array) => array[Math.floor(Math.random() * array.length)];

const resolvers = {
  StageDesk: {
    stages: () => ([
      { name: 'todo' },
      { name: 'doing' },
      { name: 'under_review' },
      { name: 'done' },
    ]),
  },
};

const server = new ApolloServer({
  typeDefs: [DateTimeTypeDefinition, typeDefs],
  resolvers,
  mockEntireSchema: false,
  mocks: {
    DateTime: () => new Date().toISOString(),
    Issue: {
      priority: () => Math.floor(Math.random() * 4),
      name: () => casual.title,
      description: () => casual.text,
      createdAt: () => casual.moment.toISOString(),
      version: () => casual.integer(0, 10),
      historyLog: () => new MockList(10),
    },
    Stage: {
      name: () => randomItem(['todo', 'doing', 'under_review', 'done']),
    },
    History: {
      type: () => randomItem(['add_executor', 'add_sprint', 'change', 'create', 'start']),
    },
    Query: () => ({
      getIssues: () => new MockList(10),
    }),
  },
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
