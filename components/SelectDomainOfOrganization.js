import { useQuery, gql } from '@apollo/client';

import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

const GET_DOMAINS = gql(`
query GetDomainsOfOrganization($domainId: ID!) {
    getDomainsOfOrganization(domainId: $domainId) {
      id
      name
      subDomains {
        id
        name
      }
      domainType
    }
  }
`);
function SelectDomainOfOrganization(props) {
  const domains = useQuery(GET_DOMAINS, {
    variables: { domainId: props.id },
  });
  if (domains.loading) return null;

  return (
    <Select
      sx={{ minWidth: 250 }}
      {...props}
    >
      {domains.data.getDomainsOfOrganization.map((domain) => {
        if (domain.domainType !== 'organization') return [];
        return [
          <MenuItem
            key={domain.id}
            value={domain.id}
          >
            {domain.name}
          </MenuItem>,
          domain.subDomains.map((subDomain) => (
            <MenuItem
              key={subDomain.id}
              value={subDomain.id}
              sx={{ paddingLeft: '30px' }}
            >
              {subDomain.name}
            </MenuItem>
          )),
        ];
      })}
    </Select>
  );
}

export default SelectDomainOfOrganization;
