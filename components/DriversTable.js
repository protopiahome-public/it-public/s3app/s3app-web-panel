import {
  Table, TableBody, TableCell, TableHead, TableRow,
} from '@mui/material';
import Link from 'next/link';

const order = {
  '+': 0,
  0: 1,
  '-': 2,
};

function DriversTable({ drivers, icons }) {
  const groupedDrivers = drivers.reduce((acc, driver) => {
    acc[order[driver.driverEffect]].push(driver);
    return acc;
  }, [[], [], []]);

  return (
    <Table className="table-fixed" size="small">
      <TableHead>
        <TableRow>
          <TableCell align="center">{icons?.['+'].icon || 'Положительная'}</TableCell>
          <TableCell align="center">{icons?.['0'].icon || 'Нейтральная'}</TableCell>
          <TableCell align="center">{icons?.['-'].icon || 'Негативная'}</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          {groupedDrivers.map((_drivers, index) => (
            <TableCell key={index} align="left">
              {_drivers.map((driver) => (
                <p key={driver.id}>
                  <Link href={`/domains/${driver.domain.id}/drivers/${driver.id}`}>
                    {driver.name}
                  </Link>
                </p>
              ))}
            </TableCell>
          ))}
        </TableRow>
      </TableBody>
    </Table>
  );
}

export default DriversTable;
