import { Box } from '@mui/material';

const styles = {
  leftBlock: (theme) => ({
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    ...theme.variables.leftPanel,
  }),
  leftBlockHeader: (theme) => ({
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.primary.contrastText,
    height: 72,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  }),
};

function LeftSubPanel({ children }) {
  return (
    <Box sx={styles.leftBlock}>
      {children}
    </Box>
  );
}

export default LeftSubPanel;
