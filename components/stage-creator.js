import { useState } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

const GET_STAGES = gql`
query GetDomain($getDomainId: ID!) {
  getDomain(id: $getDomainId) {
    stages {
      id
      name
    }
  }
}
`;

const CREATE_STAGE = gql`
mutation CreateStage($stage: StageInput!) {
  createStage(stage: $stage) {
    id
  }
}`;

const DELETE_STAGE = gql`
mutation Mutation($deleteStageId: ID!) {
  deleteStage(id: $deleteStageId)
}`;

const EDIT_STAGE = gql`
mutation Mutation($editStageId: ID!, $stage: StageInput!) {
  editStage(id: $editStageId, stage: $stage) {
    id
  }
}`;

function StageCreator() {
  const router = useRouter();

  const { enqueueSnackbar } = useSnackbar();

  const { data, refetch } = useQuery(GET_STAGES, { variables: { getDomainId: router.query.id } });

  const [selectedStage, setSelectedStage] = useState('');

  const [createStage] = useMutation(CREATE_STAGE);
  const [editStage] = useMutation(EDIT_STAGE);
  const [deleteStage] = useMutation(DELETE_STAGE);

  const [open, setOpen] = useState(false);
  const [newStage, setNewStage] = useState('', [open]);

  const handleListItemClick = (event, stage) => {
    setSelectedStage(stage);
  };
  return (
    <>
      <Box sx={{ marginTop: 3, marginLeft: 3, display: 'flex' }}>
        <Box sx={{ width: 250 }}>
          <List
            sx={{
              bgcolor: 'background.paper',
              position: 'relative',
              overflow: 'auto',
              maxHeight: 300,
              '& ul': { padding: 0 },
              border: '2px solid #F79244',
              borderRadius: 1,
              padding: 0,
            }}
          >
            {data?.getDomain?.stages.length ? (
              data.getDomain?.stages.map((stage) => (
                <ListItemButton
                  selected={selectedStage.id === stage.id}
                  key={stage.id}
                  onClick={(event) => handleListItemClick(event, stage)}
                >
                  <ListItemText primary={stage.name} />
                </ListItemButton>
              ))
            ) : (
              <ListItemButton disabled>
                <ListItemText primary="Этапов нет" />
              </ListItemButton>
            )}
          </List>
          <Button
            variant="contained"
            sx={{ marginTop: 1, width: '100%' }}
            onClick={() => {
              setOpen(true);
            }}
          >
            Создать этап
          </Button>
        </Box>
        <Box
          sx={{
            marginLeft: 1,
            display: selectedStage ? 'block' : 'none',
            maxWidth: 370,
          }}
        >
          <TextField
            required
            variant="outlined"
            size="small"
            label="Название"
            value={selectedStage.name}
            sx={{ width: '100%' }}
            onChange={(e) => setSelectedStage({ ...selectedStage, name: e.target.value })}
          />
          <Button
            variant="contained"
            sx={{ marginTop: 1, width: '100%' }}
            onClick={async () => {
              try {
                await editStage({
                  variables: {
                    editStageId: selectedStage.id,
                    stage: {
                      name: selectedStage.name,
                    },
                  },
                }).then(() => {
                  refetch();
                });
              } catch (e) {
                console.log(e);
                enqueueSnackbar(e.message, { variant: 'error' });
                return;
              }
              enqueueSnackbar('Этап успешно отредактирован', { variant: 'success' });
            }}
          >
            Сохранить
          </Button>
          <Button
            variant="contained"
            sx={{
              marginTop: 1, width: '100%', background: 'red', '&:hover': { background: 'firebrick' },
            }}
            onClick={async () => {
              try {
                await deleteStage({
                  variables: {
                    deleteStageId: selectedStage.id,
                  },
                }).then(() => {
                  refetch();
                });
              } catch (e) {
                console.log(e);
                enqueueSnackbar(e.message, { variant: 'error' });
                return;
              }
              setSelectedStage('');
              enqueueSnackbar('Этап успешно удален', { variant: 'success' });
            }}
          >
            Удалить
          </Button>
        </Box>
      </Box>
      <Dialog
        open={open}
        onClose={() => {
          setOpen(false);
          setNewStage('');
        }}
      >
        <DialogTitle>Создание этапа</DialogTitle>
        <DialogContent>
          <DialogContentText sx={{ maxWidth: 350 }}>
            Этап - часть пути формирования конечного результата
          </DialogContentText>

          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Название"
            type="text"
            fullWidth
            variant="standard"
            onChange={(e) => setNewStage(e.target.value)}
          />

          <DialogActions>
            <Button
              variant="contained"
              onClick={() => {
                setOpen(false);
              }}
            >
              Отменить
            </Button>
            <Button
              variant="contained"
              disabled={!newStage}
              onClick={async () => {
                try {
                  await createStage({
                    variables: {
                      stage: { parentDomain: router.query.id, name: newStage },
                    },
                  }).then(() => {
                    refetch();
                    setOpen(false);
                  });
                } catch (e) {
                  console.log(e);
                  enqueueSnackbar(e.message, { variant: 'error' });
                  return;
                }
                enqueueSnackbar('Этап успешно создан', { variant: 'success' });
                setOpen(false);
              }}
            >
              Создать
            </Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </>
  );
}

export default StageCreator;
