import { Tooltip, Avatar } from '@mui/material';
import Color from 'color';

const strToColor = (str) => {
  if (!str) return '#fff';

  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < str.length; i += 1) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = '#';

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
};

export default function S3Avatar(props) {
  if (!props.name) return null;

  const color = strToColor(props.name);

  return (
    <Tooltip title={props.tooltip ? props.name : ''}>
      <Avatar
        sx={{
          bgcolor: props.color || color,
          color: Color(color).luminosity() > 0.6 ? 'black' : 'white',
          borderRadius: '50%',
          borderWidth: '2px',
          borderStyle: props.noBorder ? 'none' : 'solid',
          borderColor: (theme) => theme.palette.primary.main,
          width: props.size || 36,
          height: props.size || 36,
          ...props.sx,
        }}
        onClick={props.onClick}
      >
        <span style={{ fontSize: '85%' }}>
          {props.name?.toUpperCase()
            .split(' ')
            .slice(0, 2)
            .map((word) => word.charAt(0))
            .join('')}
        </span>
      </Avatar>
    </Tooltip>
  );
}
