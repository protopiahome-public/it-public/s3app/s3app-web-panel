import Search from '@mui/icons-material/Search';
import {
  InputAdornment, TextField,
} from '@mui/material';

function SearchPanel(props) {
  return (
    <div style={{
      display: 'flex', height: 72, alignItems: 'center', padding: '0px 32px', borderBottom: '1px solid #DAE5F4',
    }}
    >
      <div style={{ flex: 1, marginRight: '20px' }}>
        <TextField
          size="small"
          fullWidth
          variant="outlined"
          value={props.search}
          placeholder="Поиск"
          onChange={(e) => props.setSearch(e.target.value)}
          sx={{
            '&': (theme) => ({
              backgroundColor: theme.palette.secondary.main,
              color: theme.palette.secondary.contrastText,
            }),
          }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Search />
              </InputAdornment>
            ),
            sx: {
              '& fieldset': {
                borderWidth: 0,
              },
              '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
                borderWidth: 0,
              },
            },
          }}
        />
      </div>
      {props.children}
    </div>
  );
}

export default SearchPanel;
