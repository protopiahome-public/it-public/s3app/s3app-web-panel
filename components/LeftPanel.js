import { Box, SvgIcon } from '@mui/material';

import Domain from '../assets/domain.svg';
import Circles from '../assets/circles.svg';
import Chats from '../assets/chats.svg';
import News from '../assets/news.svg';
import Settings from '../assets/settings.svg';
import Search from '../assets/search.svg';

const buttons = [
  { Image: Domain, name: 'domain' },
  { Image: Circles, name: 'circles' },
  { Image: Chats, name: 'chats' },
  { Image: News, name: 'news' },
  { Image: Settings, name: 'settings' },
  { Image: Search, name: 'search' },
];

function LeftPanel({ selected }) {
  return (
    <div style={{
      display: 'flex', flexDirection: 'column',
    }}
    >
      {buttons.map((button, key) => (
        <Box
          key={key}
          sx={(theme) => ({
            width: 72,
            height: 72,
            backgroundColor: button.name === selected && theme.palette.primary.main,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            border: `1px solid ${theme.palette.secondary.main}`,
            boxSizing: 'border-box',
          })}
        >
          <SvgIcon
            component={button.Image}
            inheritViewBox
            sx={{
              width: 48,
              height: 48,
              color:
            (theme) => (button.selected ? '#ffffff' : theme.palette.secondary.contrastText),
            }}
          />
          {/* <SvgIcon style={{
            width: 48, height: 48, color: 'green',
          }}
          >
            <button.Image />
          </SvgIcon> */}
        </Box>
      ))}
    </div>
  );
}

export default LeftPanel;
