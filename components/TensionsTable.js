import {
  Table, TableBody, TableCell, TableHead, TableRow,
} from '@mui/material';
import Link from 'next/link';

const order = {
  '+': 0,
  0: 1,
  '-': 2,
};

function TensionsTable({ tensions, icons }) {
  const groupedTensions = tensions.reduce((acc, tension) => {
    acc[order[tension.tensionMark]].push(tension);
    return acc;
  }, [[], [], []]);

  return (
    <Table className="table-fixed" size="small">
      <TableHead>
        <TableRow>
          <TableCell align="center">{icons?.['+'].icon || 'Положительная'}</TableCell>
          <TableCell align="center">{icons?.['0'].icon || 'Нейтральная'}</TableCell>
          <TableCell align="center">{icons?.['-'].icon || 'Негативная'}</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          {groupedTensions.map((_tensions, index) => (
            <TableCell key={index} align="left">
              {_tensions.map((tension) => (
                <p key={tension.id}>
                  <Link href={`/domains/${tension.domain.id}/tensions/${tension.id}`}>
                    {tension.name}
                  </Link>
                </p>
              ))}
            </TableCell>
          ))}
        </TableRow>
      </TableBody>
    </Table>
  );
}

export default TensionsTable;
