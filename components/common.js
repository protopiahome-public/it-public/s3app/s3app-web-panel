import { Box } from '@mui/material';
import { DragHandleOutlined, ThumbDownAltOutlined, ThumbUpAltOutlined } from '@mui/icons-material';
import Shield from '../assets/shield.svg';
import ExampleImage from '../assets/example-image.png';

export const priorities = [
  { color: '#687B98', name: 'Низкий' },
  { color: '#F79244', name: 'Средний' },
  { color: '#EF4444', name: 'Высокий' },
];

export const priorityShield = (priority) => (
  <Shield style={{ stroke: priorities[priority]?.color }} />
);

export function RoundedImage(props) {
  return (
    <Box
      {...props}
      component="img"
      src={ExampleImage.src}
      sx={{
        borderRadius: '50%',
        borderWidth: '2px',
        borderStyle: 'solid',
        borderColor: (theme) => theme.palette.primary.main,
      }}
    />
  );
}

export function checkEmpty(value) {
  return value === '' || value === null || value === undefined;
}

export function domainsToHierarchy(domains) {
  domains.forEach((domain) => {
    // eslint-disable-next-line no-param-reassign
    domain.subDomains = domains.filter((subDomain) => subDomain.parentDomain?.id === domain.id);
  });
  return domains.filter((domain) => domain.domainType === 'organization');
}

export const effects = {
  '+': {
    icon: <ThumbUpAltOutlined className="size-7 text-green-500" />,
    label: 'Положительная',
    labelDriver: 'Положительное',
  },
  0: {
    icon: <DragHandleOutlined className="size-7 text-tertiary" />,
    label: 'Нейтральная',
    labelDriver: 'Нейтральное',
  },
  '-': {
    icon: <ThumbDownAltOutlined className="size-7 text-red-500" />,
    label: 'Негативная',
    labelDriver: 'Негативное',
  },
};

export const driverStates = {
  need: {
    label: 'Потребность',
  },
  goal: {
    label: 'Цель',
  },
  satisfied: {
    label: 'Удовлетворен',
  },
  irrelevant: {
    label: 'Неактуален',
  },
};

export const proposalStatuses = {
  new: {
    label: 'Новое',
  },
  accepted: {
    label: 'Принято',
  },
  holded: {
    label: 'Отложено',
  },
  rejected: {
    label: 'Отклонено',
  },
  ceased: {
    label: 'Утратило силу',
  },
  review: {
    label: 'Требуется пересмотр',
  },
};
