import mix from 'mix-color';

function VerticalProgressbar(props) {
  const color = mix('#859CBE', '#F2B687', props.percent / 100);
  return (
    <div style={{ position: 'relative' }}>
      {new Array(8).fill(0).map((_, index) => (
        <div
          key={index}
          style={{
            width: '32px',
            height: '5px',
            borderRadius: '2px',
            backgroundColor: '#DAE5F4',
            marginBottom: '2px',
          }}
        />
      ))}
      <div style={{
        position: 'absolute',
        height: `${props.percent}%`,
        bottom: '0px',
        width: '32px',
        backgroundColor: color,
        zIndex: 2,
      }}
      />
    </div>
  );
}

export default VerticalProgressbar;
