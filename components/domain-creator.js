import { useState, useEffect } from 'react';
import { gql, useQuery, useMutation } from '@apollo/client';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';

import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import Plus from '../assets/plus.svg';
import SelectDomain from './SelectDomain';

function DomainCreator(props) {
  const router = useRouter();

  const GET_DOMAINS = gql`
        query getDomains {
          getDomains {
            id
            name
          }
        }`;
  const CREATE_DOMAIN = gql`
        mutation createDomain($domain: DomainInput!) {
          createDomain(domain: $domain) {
            name
            id
            parentDomain {
              id
            }
            participants {
              id
            }
          }
        }`;
  const { enqueueSnackbar } = useSnackbar();

  const [createDomainForm, setCreateDomainForm] = useState({
    name: '',
    parentDomainId: '',
    open: false,
  });
  const [createDomain] = useMutation(CREATE_DOMAIN);
  const domains = useQuery(GET_DOMAINS);

  useEffect(() => {
    if (props.refetch) domains.refetch();
  }, [props.refetch, domains]);

  let title = 'круга';
  if (props.organization) {
    title = 'организации';
  }
  if (props.role) {
    title = 'роли';
  }

  let description = 'Круг - четко определенная зона влияния, деятельности и принятия решений внутри организации';
  if (props.organization) {
    description = 'Организация - группа людей с общей целью, где власть и решения распределены равномерно на основе компетенций и задач';
  }
  if (props.role) {
    description = 'Роль - зона влияния, деятельности и принятия решений, предоставленная участнику круга';
  }

  let type = 'circle';
  if (props.organization) {
    type = 'organization';
  }
  if (props.role) {
    type = 'role';
  }

  return (
    <>
      {props.plus ? (
        <Plus style={{ marginRight: '24px', cursor: 'pointer' }} onClick={() => setCreateDomainForm({ ...createDomainForm, open: true })} />
      ) : (
        <Button variant="contained" onClick={() => setCreateDomainForm({ ...createDomainForm, open: true })} fullWidth style={props.style}>
          Создание&nbsp;
          {title}
        </Button>
      )}
      <Dialog
        open={createDomainForm.open}
        onClose={() => {
          setCreateDomainForm({
            ...createDomainForm, name: '', parentDomainId: '', open: false,
          });
        }}
      >
        <DialogTitle>
          Создание&nbsp;
          {title}
        </DialogTitle>
        <DialogContent>

          <DialogContentText sx={{ maxWidth: 350 }}>
            {description}
          </DialogContentText>

          <TextField
            margin="dense"
            label="Название"
            type="text"
            fullWidth
            variant="standard"
            value={createDomainForm.name}
            onChange={(e) => setCreateDomainForm({ ...createDomainForm, name: e.target.value })}
          />
          {/*
            <TextField
              autoFocus
              multiline
              minRows="4"
              margin="dense"
              id="name"
              label="Описание"
              fullWidth
              variant="standard"
              onChange={(e) =>
                setCreateDomainForm({ ...createDomainForm, description: e.target.value })}
            />
            */}

          {props.organization ? '' : (
            <>
              <InputLabel>Родительский домен</InputLabel>
              <SelectDomain
                value={createDomainForm.parentDomainId}
                disabled={!domains.data?.getDomains.length}
                variant="outlined"
                size="small"
                fullWidth
                filter={(item) => item.domain.domainType !== 'role'}
                onChange={(e) => setCreateDomainForm({
                  ...createDomainForm,
                  parentDomainId: e.target.value,
                })}
              />
            </>
          )}
          <DialogActions sx={{ px: 0 }}>
            <Button
              variant="contained"
              onClick={() => {
                setCreateDomainForm({
                  ...createDomainForm, name: '', parentDomainId: '', open: false,
                });
              }}
            >
              Отменить
            </Button>
            <Button
              variant="contained"
              disabled={(!createDomainForm.name)
                 || (!props.organization && !createDomainForm.parentDomainId)}
              onClick={async () => {
                try {
                  if (!createDomainForm.parentDomainId) createDomainForm.parentDomainId = undefined;
                  const res = await createDomain({
                    variables: {
                      domain: {
                        name: createDomainForm.name,
                        parentDomainId: createDomainForm.parentDomainId,
                        participants: [props.user.id],
                        domainType: type,
                      },
                    },
                  });
                  await props.refetchUser();
                  router.push(`/domains/${res.data.createDomain.id}`);
                  setCreateDomainForm({ ...createDomainForm, open: false });
                } catch (e) {
                  console.log(e);
                  enqueueSnackbar(e.message, { variant: 'error' });
                  return;
                }
                enqueueSnackbar('Домен успешно создан', { variant: 'success' });
                setCreateDomainForm({ ...createDomainForm, open: false });
              }}
            >
              Создать
            </Button>
          </DialogActions>
        </DialogContent>

      </Dialog>
    </>
  );
}

export default DomainCreator;
