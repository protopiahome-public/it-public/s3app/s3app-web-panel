// import type { CodeBlockEditorDescriptor } from '@mdxeditor/editor';
// eslint-disable-next-line import/no-unresolved

'use client';

import {
  headingsPlugin, imagePlugin, linkDialogPlugin, linkPlugin, listsPlugin,
  markdownShortcutPlugin, quotePlugin,
} from '@mdxeditor/editor';
// eslint-disable-next-line import/no-unresolved
import '@mdxeditor/editor/style.css';

const {
  MDXEditor,
} = await import('@mdxeditor/editor');
// import { MDXEditor } from '@mdxeditor/editor';
function MarkdownEditor({ value }) {
  return (
    <>
      <style>
        {`.markdown-preview {
        padding-top: 0px;
        padding-bottom: 0px;
      }`}
      </style>
      <MDXEditor
        readOnly
        markdown={value}
        plugins={
        [
          linkDialogPlugin(),
          headingsPlugin(),
          listsPlugin(),
          linkPlugin(),
          quotePlugin(),
          markdownShortcutPlugin(),
          imagePlugin({ disableImageSettingsButton: true }),
        ]
}
        contentEditableClassName="markdown-preview"
      />
    </>
  );
}

export default MarkdownEditor;
