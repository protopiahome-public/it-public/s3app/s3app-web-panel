import { SvgIcon } from '@mui/material';

import { useRouter } from 'next/router';
import Link from 'next/link';
import {
  CalendarMonthOutlined,
  DoubleArrowOutlined,
  HandshakeOutlined,
  DateRange,
  HubOutlined,
  TaskAltOutlined,
  GroupWorkOutlined,
  PeopleAltOutlined,
  ArrowOutward,
  Psychology,
  FormatListBulleted,
  FactCheckOutlined,
} from '@mui/icons-material';

const items = {
  domain: {
    title: 'Домен',
    Image: GroupWorkOutlined,
    url: '',
  },
  issues: {
    title: 'Задачи',
    Image: TaskAltOutlined,
    url: '/issues',
  },
  calendar: {
    title: 'Календарь',
    Image: CalendarMonthOutlined,
    url: '/calendar',
  },
  thoughts: {
    title: 'Мысли и идеи',
    Image: Psychology,
    url: '/thoughts',
  },
  tensions: {
    title: 'Наблюдения и факты',
    Image: FactCheckOutlined,
    url: '/tensions',
  },
  drivers: {
    title: 'Потребности и цели',
    Image: ArrowOutward,
    url: '/drivers',
  },
  proposals: {
    title: 'Предложения и соглашения',
    Image: HandshakeOutlined,
    url: '/proposals',
  },
  values: {
    title: 'Создание ценности',
    Image: DoubleArrowOutlined,
    url: '/values',
  },
  'issue-types': {
    title: 'Типы задач',
    Image: FormatListBulleted,
    url: '/issue-types',
  },
  sprints: {
    title: 'Спринты',
    Image: DateRange,
    url: '/sprints',
  },
  domainMap: {
    title: 'Карта доменов',
    Image: HubOutlined,
    url: '/domains',
  },
  participants: {
    title: 'Участники организации',
    Image: PeopleAltOutlined,
    url: '/participants',
  },
};

export default function DomainLeftPanel({ selected }) {
  const router = useRouter();
  const domainPrefix = `/domains/${router.query.id}`;

  return Object.keys(items).map((key) => (
    <Link href={domainPrefix + items[key].url} key={key}>
      <div
        style={{
          width: '100%',
          height: 36,
          border: '1px solid #DAE5F4',
          display: 'flex',
          justifyContent: 'left',
          alignItems: 'center',
          padding: 16,
          cursor: 'pointer',
          boxSizing: 'border-box',
          gap: 12,
          backgroundColor: key === selected ? '#F79244' : '#FFFFFF',
          color: key === selected ? '#FFFFFF' : '#687B98',
        }}
      >
        <SvgIcon
          component={items[key].Image}
          inheritViewBox
          sx={{
            width: 24,
            height: 24,
            color:
            (theme) => (key === selected ? '#ffffff' : theme.palette.secondary.contrastText),
          }}
        />
        {items[key].title}
      </div>
    </Link>
  ));
}
