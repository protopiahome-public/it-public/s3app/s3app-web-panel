import { useRouter } from 'next/router';
import {
  Button, MenuItem, Select,
} from '@mui/material';

function IssuesPanel(props) {
  const router = useRouter();
  const issuesPrefix = router.asPath.split('/').slice(0, 4).join('/');

  return (
    <span
      style={{
        marginLeft: 'auto',
        display: 'inline-flex',
        alignItems: 'center',
        color: '#A2BDE4',
        fontSize: '16px',
      }}
    >
      Вид
      <Select defaultValue="issues" value={props.page} variant="standard" style={{ marginLeft: '20px' }}>
        <MenuItem
          value="issues"
          onClick={() => router.push(issuesPrefix)}
        >
          Список проблем
        </MenuItem>
        <MenuItem
          value="kanban"
          onClick={() => router.push(`${issuesPrefix}/kanban`)}
        >
          Канбан-доска
        </MenuItem>
      </Select>
      <Button
        color="secondary"
        variant="contained"
        style={{ marginLeft: '20px' }}
        onClick={() => router.push(`/domains/${router.query.id}/sprints/create`)}
      >
        Добавить спринт
      </Button>
      <Button
        variant="contained"
        style={{ marginLeft: '20px' }}
        onClick={() => {
          if (props.setCreateDialogOpen) {
            props.setCreateDialogOpen(true);
          } else {
            router.push(`${issuesPrefix}/create`);
          }
        }}
      >
        Новая задача
      </Button>
    </span>
  );
}

export default IssuesPanel;
