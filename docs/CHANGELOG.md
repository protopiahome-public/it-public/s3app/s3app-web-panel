## [0.13.1](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.13.0...v0.13.1) (2024-03-11)


### Bug Fixes

* Исправить ошибку с регистрацией ([b909b17](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/b909b171c71b250f57f4681f64c35445b99accf1))

# [0.13.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.12.0...v0.13.0) (2024-02-06)


### Features

* Отображение спринтов в календаре ([53b00fe](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/53b00fe192b64926808e8fe831912bbd2fbbbea3))

# [0.12.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.11.1...v0.12.0) (2024-01-21)


### Features

* Интеграция с репозиторием лицензий ([75f64fa](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/75f64fae74adc248cea3d7b1feeb9af2de22abcb))

## [0.11.1](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.11.0...v0.11.1) (2024-01-19)


### Bug Fixes

* email field ([e4c1c84](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/e4c1c842ae6ecafbfc1e4a652f7f6adf8b1e87b2))

# [0.11.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.10.0...v0.11.0) (2024-01-15)


### Features

* Адаптировать для postgres версии ([30d174c](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/30d174c55d98886b8e1c156ea66ccdb60cbb741a))

# [0.10.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.9.1...v0.10.0) (2024-01-13)


### Features

* Оптимизация ci ([15f1766](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/15f176642ae452111fb802917375613b57d5e9ab))

## [0.9.1](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.9.0...v0.9.1) (2023-12-28)


### Bug Fixes

* Ошибка при переименовании домена ([592ac48](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/592ac4805f6a559a430398011bec5a3ea5de2ad0))

# [0.9.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.8.1...v0.9.0) (2023-12-28)


### Features

* Редактирование домена ([c4679d8](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/c4679d8acbff246c3f025ab86813f20de677f6f9))

## [0.8.1](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.8.0...v0.8.1) (2023-12-13)


### Bug Fixes

* Не работает подтверждение email ([ae67adb](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/ae67adb2a17515a1323077d49be0cf3be4db44d8))

# [0.8.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.7.0...v0.8.0) (2023-12-13)


### Features

* Добавить подтверждение email ([e8d589f](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/e8d589fff7cbd95c837419c36fc8a2211a3aab90))

# [0.7.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.6.1...v0.7.0) (2023-12-03)


### Features

* Страницы аккаунта, восстановления и изменения пароля ([521862c](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/521862c12bcf4b88b280b3b90b9f85da780702a2))

## [0.6.1](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.6.0...v0.6.1) (2023-12-02)


### Bug Fixes

* Обязательные поля для заполнения ([056e859](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/056e859d4725daf8afef4d1b2fc8f606e2f7c92a))

# [0.6.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.5.3...v0.6.0) (2023-11-01)


### Features

* Публичная докеризация ([1853717](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/1853717999d3b9a2a61abade70aa0075a6622fa3))

## [0.5.3](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.5.2...v0.5.3) (2023-10-30)


### Bug Fixes

* Не получается создать первую организацию ([2a955ae](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/2a955ae4bb8ec14f27116e8ede8b123a608a57e6))

## [0.5.2](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.5.1...v0.5.2) (2023-10-25)


### Bug Fixes

* Рефакторинг и доработки фронтенда ([0c950ee](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/0c950eee22cd658389987f14baabc934666e7f15))

## [0.5.1](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.5.0...v0.5.1) (2023-10-16)


### Bug Fixes

* Минимальная ширина на мобильных устройствах ([33cbcd0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/33cbcd065de19882ff44940d415d3c37aa78a92c))

# [0.5.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.4.6...v0.5.0) (2023-10-16)


### Features

* Доработки для демо ([e84fa54](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/e84fa541010ad9b1355651bab5b2a79214a670e0))

## [0.4.6](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.4.5...v0.4.6) (2023-10-01)


### Bug Fixes

* Исправление отображения доменов в навбаре ([e3d47a6](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/e3d47a6370f94e9aa602262029596a118ee2c389))

## [0.4.5](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.4.4...v0.4.5) (2023-09-26)


### Bug Fixes

* Исправления на странице канбан-доски ([35d15ed](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/35d15ed73f03a4e773186aebbd56c71adc52b449))

## [0.4.4](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.4.3...v0.4.4) (2023-09-24)


### Bug Fixes

* Исправление ошибки при создании задачи ([b0f15dd](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/b0f15dd07327aaa56de854d81949953efebed40f)), closes [#33](https://gitlab.com/s3app-team/s3app/s3app-web-panel/issues/33)

## [0.4.3](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.4.2...v0.4.3) (2023-09-18)


### Bug Fixes

* Исправления в domain-creator ([741442b](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/741442be86bd7389c0fa0b920e8d870101c5bf05))

## [0.4.2](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.4.1...v0.4.2) (2023-09-15)


### Bug Fixes

* Рефакторинг компонентов, исправления в карте доменов ([9c4cbc5](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/9c4cbc5ecf1301ab847914ea1bd1854be74703ca))

## [0.4.1](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.4.0...v0.4.1) (2023-09-08)


### Bug Fixes

* Замена заглушки изображения для домена ([f4f3606](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/f4f3606b6c4b28d54263b6bb1147cd7eb608fdfa))

# [0.4.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.3.0...v0.4.0) (2023-09-07)


### Features

* Аватары пользователей и общие исправления ([9525d4d](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/9525d4dbe4a02413e9c18d2b5d263c2983b4f4b3))

# [0.3.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.2.0...v0.3.0) (2023-09-01)


### Features

* Доработка спринтов и общие исправления ([9b8d15e](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/9b8d15ee9a6f6249c70b469faf7c2cf56bb1ed42))

# [0.2.0](https://gitlab.com/s3app-team/s3app/s3app-web-panel/compare/v0.1.0...v0.2.0) (2023-08-08)


### Features

* Отображение кругов и ролей ([28d9dbe](https://gitlab.com/s3app-team/s3app/s3app-web-panel/commit/28d9dbe1eb63f9919c08563ee8ffef66ff81d5cb))
